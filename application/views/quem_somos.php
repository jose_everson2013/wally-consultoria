<body id="quem_somos">

<div id="container">
	<div id="top">
    	<div id="logo"><a href="<?php echo site_url(); ?>"><img src="<?php echo url_img('logo.png'); ?>" alt="Wally Consultoria" /></a></div>
        <div id="text_top">Expertise e<br /><span>Conhecimento</span></div>
        <div class="clear"></div>
    </div>
    
    <?php echo $menu;?>
    
    <div id="face"><a href=""><img src="<?php echo url_img('face.png'); ?>" alt="Facebook" /></a></div>
    <div id="phone"><span>11</span> 2847.4553</div>
    <div class="clear"></div>
    <div id="restricted">Área Restrita
    	<form method="post" action="<?php echo site_url('home/login');?>">
            <input type="text" name="usuario" id="login" placeholder="Seu login" />
            <input type="password" name="senha" id="senha" placeholder="Sua senha" />
            <input type="image" src="<?php echo url_img('btn.png'); ?>" id="button_login" alt="Logar" />             
        </form>
        <a href="<?php echo site_url(array('admin/home', 'recuperar_senha'));?>">Esqueci minha senha.</a>
    </div>
    <div id="banner">
    	<a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" alt="" /></a>
        <a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" alt="" /></a>
    </div>
    <div id="content_top">
    	<span style="margin-left: 10px;">Quem <br /> Somos</span>
        <div id="content_text" style="font-size: 18px;width: 708px;">A Wally Consultoria é uma empresa especializada na prestação de serviços de assessoria e consultoria técnica e atuarial, focada no segmento de operadoras de planos privados de assistência à saúde. <br /><br />

Sua expertise é resultado da atuação e da ampla experiência de seus profissionais nos segmentos de Saúde Suplementar. A Wally Consultoria tem atuação nacional e conta com profissionais qualificados e preparados para prestar atendimento de forma personalizada, apresentando orientações e soluções técnicas que levam em consideração as características e as necessidades de cada cliente.
		</div>
    </div>
    
    <div class="bar"></div>
    <div class="clear"></div>
    <div id="empresa">
    	<p id="missao">Oferecer serviços com elevado valor agregado e soluções eficazes,<br />
sempre focados nas necessidades de nossos clientes.</p>
		<p id="visao">Representarmos uma excelência na assessoria e conhecimento<br />
do setor de saúde suplementar.</p>
		<p id="valores">Capital Intelectual, Inovação, Comprometimento e Agilidade.</p>
    </div>
    <div id="footer">
    	<div id="address">
        	<b>Av. Paulista, 2300 – Andar Pilotis – Bela Vista</b>
			CEP: 01310-300 - São Paulo - SP
		</div>
        <div id="copyright">
        	<span><b>Wally Consultoria -</b> 2013 - Todos os direitos reservados</span>
        </div>
        <div id="development">
        	<a href="http://www.kmcnigro.com.br" target="_blank"><img src="<?php echo url_img('kmcnigro.png'); ?>" alt="KMC Nigro" /></a>
        </div>
        <div class="clear"></div>
    </div>
</div>
