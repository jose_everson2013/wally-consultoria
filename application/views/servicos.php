<body>

<div id="container">
	<div id="top">
    	<div id="logo"><a href="<?php echo site_url(); ?>"><img src="<?php echo url_img('logo.png'); ?>" alt="Wally Consultoria" /></a></div>
        <div id="text_top">Expertise e<br /><span>Conhecimento</span></div>
        <div class="clear"></div>
    </div>
    
    <?php echo $menu;?>
    
    <div id="face"><a href=""><img src="<?php echo url_img('face.png'); ?>" alt="Facebook" /></a></div>
    <div id="phone"><span>11</span> 2847.4553</div>
    <div class="clear"></div>
    <div id="restricted">Área Restrita
    	<form method="post" action="<?php echo site_url('home/login');?>">
            <input type="text" name="usuario" id="login" placeholder="Seu login" />
            <input type="password" name="senha" id="senha" placeholder="Sua senha" />
            <input type="image" src="<?php echo url_img('btn.png'); ?>" id="button_login" alt="Login" />
            <a href="<?php echo site_url(array('admin/home', 'recuperar_senha'));?>">Esqueci minha senha.</a>
        </form>
    </div>
    <div id="banner">
    	<a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" alt="" /></a>
        <a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" alt="" /></a>
    </div>
    <div id="content_top">
    	<span>Serviços</span>
        <div id="content_text">Soluções que atendam às necessidades dos clientes e do mercado de saúde suplementar <b>tendo como principais serviços:</b></div>
    </div>
    <div id="balls">
    
    </div>
    <div id="content_bottom">
    	Desenvolvimento de estudos técnicos para instituição de novos produtos, com avaliação e fixação de preços em bases atuariais; <br />
<span>Edição e acompanhamento de Notas Técnicas de Registro de Produtos (NTRP);<br /></span>
Desenvolvimento de estudos técnicos para reavaliação de produtos e revisão de preços;<br />
<span>Elaboração e acompanhamento de Notas Técnicas Atuariais de Provisões (NTAP); <br /></span>
Elaboração e acompanhamento de relatórios gerenciais sobre custeio e operações de planos privados de assistência à saúde; <br />
<span>Desenvolvimento e implantação de sistemas de bancos de dados para controle das utilizações de serviços na área da saúde; <br /></span>
Avaliação técnica da carteira, aferindo-se os índices de frequências de utilização e custos assistenciais por evento e por beneficiário exposto ao risco, separadamente por tipo de contratação, módulo de cobertura e faixa etária.
    </div>
    <div class="clear" style="margin-bottom: -10px;"></div>
    <div id="footer">
    	<div id="address">
        	<b>Av. Paulista, 2300 – Andar Pilotis – Bela Vista</b>
			CEP: 01310-300 - São Paulo - SP
		</div>
        <div id="copyright">
        	<span><b>Wally Consultoria -</b> 2013 - Todos os direitos reservados</span>
        </div>
        <div id="development">
        	<a href="http://www.kmcnigro.com.br" target="_blank"><img src="<?php echo url_img('kmcnigro.png'); ?>" alt="KMC Nigro" /></a>
        </div>
        <div class="clear"></div>
    </div>
</div>

