<body id="informativos">

<div id="container">
	<div id="top">
    	<div id="logo"><a href="<?php echo site_url(); ?>"><img src="<?php echo url_img('logo.png'); ?>" /></a></div>
        <div id="text_top">Expertise e<br /><span>Conhecimento</span></div>
        <div class="clear"></div>
    </div>
    
    <?php echo $menu;?>
    
    <div id="face"><a href=""><img src="<?php echo url_img('face.png'); ?>" /></a></div>
    <div id="phone"><span>11</span> 2847.4553</div>
    <div class="clear"></div>
    <div id="restricted">Área Restrita
    	<form method="post" action="<?php echo site_url('home/login');?>">
            <input type="text" name="usuario" id="login" placeholder="Seu login" />
            <input type="password" name="senha" id="senha" placeholder="Sua senha" />
            <input type="image" src="<?php echo url_img('btn.png'); ?>" id="button_login" />             
        </form>
        <script type="text/javascript">
                                function makePass(){
                                   $('<input name="senha" type="password" id="senha" />').insertAfter('#senha').prev().remove();
                                   $('#senha').focus();
                                }
        </script>
    </div>
    <div class="clear"></div>
    <div id="banner">
    	<a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" /></a>
        <a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" /></a>
    </div>
</div>
<div class="clear"></div>
<div id="bgcontent">
	<div class="informativos">
    <div id="text_info">
    	<span style="font-size:48px">Informativos</span>
        <div id="content_text">Fique por dentro de tudo o que acontece acompanhando o nosso informativo!</div>
    </div>
    <?php 
	foreach ($exibe as $row) { ?>
		<div class="titulo">
			<h3><?php echo $row->titulo; ?></h3>
    	</div>
        <div class="content_info">
			<?php echo $row->conteudo; ?>
        	<a href="">Continuar lendo...</a>
        </div>
	<?php }
	?>
	</div>
</div>
<div id="bgfooter">
	<div id="footer" style="width: 980px;margin: 0 auto;position: relative;top: 30px;">
    	<div id="address">
        	<b>Av. Paulista, 2300 – Andar Pilotis – Bela Vista</b>
			CEP: 01310-300 - São Paulo - SP
		</div>
        <div id="copyright">
        	<span><b>Wally Consultoria -</b> 2013 - Todos os direitos reservados</span>
        </div>
        <div id="development">
        	<a href="http://www.kmcnigro.com.br" target="_blank"><img src="<?php echo url_img('kmcnigro.png'); ?>" /></a>
        </div>
        <div class="clear"></div>
    </div>
</div>
