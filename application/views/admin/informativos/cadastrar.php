<h2>
    <span>Cadastrar informativo</span>
</h2>
<form action="<?php echo site_url(array('admin/informativos', 'cadastrar'));?>" method="post" class="orcamento no-float">
    <?php echo validation_errors();?>
    
    <div>
        <label class="span-3">Titulo</label>
        <input type="text" name="titulo" class="span-8"  />
    </div>
    <div>
        <label class="span-3">Conteúdo</label>
        <textarea name="conteudo" class="span-8 ckeditor"></textarea>
    </div>
    <div>
        <label class="span-3">Tipo de informativo</label>
        <select name="tipo_informativo_id">
            <option value="">Selecionar tipo de informativo</option>
            <option value="site">Notícia para o site</option>
            <?php foreach ($tiposInfos as $index => $tipoInfo):?>
            	<option value="<?php echo $tipoInfo->id;?>" title="<?php echo $tipoInfo->descricao;?>"><?php echo $tipoInfo->nome;?></option>
            <?php endforeach;?>
        </select>
    </div>
    <div>
        <label class="span-3">Grupo de empresas</label>
        <select name="grupo_id">
            <option value="">Enviar para todas as empresas</option>
            <?php foreach ($gruposEmpresas as $index => $grupoEmpresa):?>
            	<option value="<?php echo $grupoEmpresa->id;?>"><?php echo $grupoEmpresa->nome;?></option>
            <?php endforeach;?>
        </select>
    </div>
    <div class="notice">
        <input type="checkbox" name="status" value="e" />
        <label class="span-3">Enviar e depois salvar informativo.</label>
    </div>
    <br />
    <div>
        <input type="submit" value="Cadastrar">
    </div>
</form>