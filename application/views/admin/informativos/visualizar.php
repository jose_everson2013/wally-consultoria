<div>
    <h3>Relatório de envio</h3>
    <?php if(!empty($destinatarios)):?>
        <h4>Destinatários</h4>
        <table>
            <thead>
                <tr>
                    <td>Id</td>
                    <td>Email</td>
                    <td>Data de envio</td>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($destinatarios as $index => $destinatario):?>
                    <tr>
                        <td><?php echo $destinatario->id; ?></td>
                        <td><?php echo $destinatario->email_destinatario; ?></td>
                        <td><?php echo $relatorioEnvio[0]->data_envio; ?></td>
                    </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    <?php else:?>
        <h4>Este informativo não possui dados de envio.</h4>
    <?php endif;?>
</div>
<table id="Table_01" width="700" border="0" cellpadding="0" cellspacing="0">
	<tr>
    	<td></td>
    </tr>
	<tr bgcolor="#0e84c4" width="782" height="30">
		<td align="center">
           <font face="'myriad Pro',calibri,verdana,tahoma,arial,sans serif" color="FFFFFF"><?php echo $titulo; ?></font>
        </td>
	</tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr class="content">
    	<td>
			<?php echo $conteudo; ?>
        </td>
    </tr>
    <tr>
    	<td align="center">Wally Consultoria - 2013</td>
    </tr>
</table>