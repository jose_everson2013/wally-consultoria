<h2>
    <span>Listagem de Informativos</span>
</h2>
<h3>Edite e exclua informativos</h3>
<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Título</th>
            <th>Data do cadastro</th>
            <th>Status</th>
            <th>Editar</th>
            <th>Excluir</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($infos as $info):?>
            <tr>
                <td><?php echo $info->id; ?></td>
                <td><?php echo $info->titulo; ?></td>
                <td><?php echo date('d/m/Y H:i:s', strtotime($info->data_cadastro));?></td>
                <td>
                    <?php if($info->status == 'r'):?>
                        Rascunho
                    <?php elseif($info->status == 'e'):?>
                        Enviado
                    <?php elseif($info->status == 's'):?>
                        Notícia do site
                    <?php endif;?>
                </td>
                <td>
                    <?php if($info->status == 'r' || $info->status == 's'):?>
                        <a href="<?php echo site_url('admin/informativos/editar/' . $info->id);?>">Editar</a>
                    <?php elseif($info->status == 'e'):?>
                        Não Editável
                    <?php endif;?>
                </td>
                <td>
                    <?php if($info->status == 'r' || $info->status == 's'):?>
                        <a href="<?php echo site_url('admin/informativos/excluir/' . $info->id);?>" onclick="return confirm('Deseja realmente excluir o informativo #<?php echo $info->id;?>?');">
                            Excluir
                        </a>
                    <?php elseif($info->status == 'e'):?>
                        <a href="<?php echo site_url('admin/informativos/visualizar/' . $info->id);?>">Visualizar</a>
                    <?php endif;?>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>