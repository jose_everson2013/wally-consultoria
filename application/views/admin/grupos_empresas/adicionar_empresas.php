<h2>
    <span>Adicionar empresas ao grupo: <?php echo $grupoEmpresa[0]->nome;?></span>
</h2>
<form action="<?php echo site_url(array('admin/grupos_empresas', 'adicionar_empresas'));?>" method="post" class="orcamento no-float">
    <?php echo validation_errors();?>
    <input type="hidden" name="grupo_id" value="<?php echo $grupoEmpresa[0]->id?>" />
    <div>
        <ul>
            <?php foreach($empresas as $empresa):?>
                <li>
                    <div>
                        <input type="checkbox" name="empresa_id[]" value="<?php echo $empresa->id;?>" <?php echo ($empresa->checked) ? 'checked="checked"' : '' ;?> />
                        <span><?php echo $empresa->razao_social; ?></span>
                        -
                        <span><?php echo $empresa->nome_fantasia; ?></span>
                    </div>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
    <div>
        <input type="submit" value="Cadastrar">
    </div>
</form>