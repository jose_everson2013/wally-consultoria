<h2>
    <span>Listagem de Grupos de empresas</span>
</h2>
<h3>Edite e adicione empresas aos grupos</h3>
<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Adicionar empresas</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($gruposEmpresas as $grupoEmpresa):?>
            <tr>
                <td><?php echo $grupoEmpresa->id; ?></td>
                <td><?php echo $grupoEmpresa->nome; ?></td>
                <td><a href="<?php echo site_url(array('admin/grupos_empresas', 'adicionar_empresas', $grupoEmpresa->id));?>">Adicionar</a></td>
                <td><a href="<?php echo site_url(array('admin/grupos_empresas', 'editar', $grupoEmpresa->id));?>">Editar</a></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>