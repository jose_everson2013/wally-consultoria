<h2>
    <span>Cadastrar Grupo de empresas</span>
</h2>
<?php if(empty($grupoEmpresa)):?>
    <form action="<?php echo site_url(array('admin/grupos_empresas', 'editar'));?>" method="post" class="orcamento no-float">
        <?php echo validation_errors();?>
        <input type="hidden" name="id" class="span-8" value="<?php echo (set_value('id')) ? set_value('id') : '';?>" />
        <div>
            <label class="span-3">Nome do grupo</label>
            <input type="text" name="nome" class="span-8" value="<?php echo (set_value('nome')) ? set_value('nome') : '';?>" />
        </div>
        <div>
            <input type="submit" value="Cadastrar">
        </div>
    </form>
<?php else:?>
    <form action="<?php echo site_url(array('admin/grupos_empresas', 'editar'));?>" method="post" class="orcamento no-float">
        <?php echo validation_errors();?>
        <input type="hidden" name="id" class="span-8" value="<?php echo (set_value('id')) ? set_value('id') : $grupoEmpresa[0]->id;?>" />
        <div>
            <label class="span-3">Nome do grupo</label>
            <input type="text" name="nome" class="span-8" value="<?php echo (set_value('nome')) ? set_value('nome') : $grupoEmpresa[0]->nome;?>" />
        </div>
        <div>
            <input type="submit" value="Cadastrar">
        </div>
    </form>
<?php endif; ?>