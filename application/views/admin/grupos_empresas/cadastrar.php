<h2>
    <span>Cadastrar Grupo de empresas</span>
</h2>
<form action="<?php echo site_url(array('admin/grupos_empresas', 'cadastrar'));?>" method="post" class="orcamento no-float">
    <?php echo validation_errors();?>
    <div>
        <label class="span-3">Nome do grupo</label>
        <input type="text" name="nome" class="span-8"  />
    </div>
    <div>
        <input type="submit" value="Cadastrar">
    </div>
</form>