<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Wally</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<table id="Table_01" width="700" border="0" cellpadding="0" cellspacing="0">
    <tr>
    	<td></td>
    </tr>
    <tr bgcolor="#0e84c4" width="782" height="30">
        <td align="center">
            <font face="'myriad Pro',calibri,verdana,tahoma,arial,sans serif" color="FFFFFF">
                Recuperar senha
            </font>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr class="content">
    	<td>
            Para recuperar sua senha, <a href="<?php echo site_url(array('admin/home', 'validar_hash', $hash)); ?>">'clique aqui'</a>. Ou copie e cole o link abaixo na barra de endereços.
            <br/>
            <br/>
            <a href="<?php echo site_url(array('admin/home', 'validar_hash', $hash)); ?>">
                <?php echo site_url(array('admin/home', 'validar_hash', $hash)); ?>
            </a>
        </td>
    </tr>
    <tr>
    	<td align="center">Wally Consultoria - 2013</td>
    </tr>
</table>
</body>
</html>