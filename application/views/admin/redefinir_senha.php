<form method="post" action="<?php echo site_url(array('admin/home', 'redefinir_senha'));?>">
    <h2><span>Redefinir senha</span></h2>
    <?php echo validation_errors();?>
    <div>
        <label class="span-3">Nova senha</label>
        <input type="password" name="nova_senha" class="span-9" required autofocus />
    </div>
    <br/>
    <div>
        <label class="span-3">Confirmar Senha</label>
        <input type="password" name="confirmar_nova_senha" class="span-9" required />
    </div>
    <br/>
    <div>
        <input type="submit" value="Redefinir"/>
    </div>
</form>