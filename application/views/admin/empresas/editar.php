<h2>
    <span>Editando Empresa</span>
</h2>
<?php if(!empty($empresa[0])):?>
    <form method="post" class="orcamento no-float" action="<?php echo site_url('admin/empresas/editar/' . $empresa[0]->id);?>">
        <?php echo validation_errors();?>
        <input type="hidden" name="id" value="<?php echo $empresa[0]->id;?>" />
        <div>
            <label class="span-3">Razão Social</label>
            <input type="text" name="razao_social" class="span-8" value="<?php echo $empresa[0]->razao_social;?>" />
        </div>
        <div>
            <label class="span-3">Nome Fantasia</label>
            <input type="text" name="nome_fantasia" class="span-8" value="<?php echo $empresa[0]->nome_fantasia;?>" />
        </div>
        <div>
            <label class="span-3">CNPJ</label>
            <input type="text" name="cnpj" class="span-8" value="<?php echo $empresa[0]->cnpj;?>" />
        </div>
        <div>
            <label class="span-3">Registro A.N.S.</label>
            <input type="text" name="registro_ans" class="span-8"  value="<?php echo $empresa[0]->registro_ans;?>"/>
        </div>
        <div>
            <label class="span-3">Categoria</label>
            <input type="text" name="categoria" class="span-8" value="<?php echo $empresa[0]->categoria;?>" />
        </div>
        <div>
            <label class="span-3">Nome do representante</label>
            <input type="text" name="nome_representante" class="span-8" value="<?php echo $empresa[0]->nome_representante;?>" />
        </div>
        <div>
            <label class="span-3">E-mail do representante</label>
            <input type="text" name="email_representante" class="span-8" value="<?php echo $empresa[0]->email_representante;?>" />
        </div>
        <div>
            <label class="span-3">Usuario</label>
            <input type="text" name="usuario" class="span-8" value="<?php echo $empresa[0]->usuario;?>" />
        </div>
        <div>
            <label class="span-3">Nova Senha (opcional)</label>
            <input type="password" name="senha" class="span-8"  />
        </div>
        <div>
            <label class="span-3">Status</label>
            <select name="status">
                <option value="a" <?php echo ($empresa[0]->status == 'a') ? 'selected="selected"' : '';?>>Ativado</option>
                <option value="d" <?php echo ($empresa[0]->status == 'd') ? 'selected="selected"' : '';?>>Desativado</option>
                <option value="b" <?php echo ($empresa[0]->status == 'b') ? 'selected="selected"' : '';?>>Bloqueado</option>
            </select>
        </div>
        <div>
            <span style="margin-top: 10px;margin-bottom: 10px;display: block;font-weight: bold;">Tipos de informativos adquiridos</span>
            <?php foreach ($tiposInformativos as $tipoInformativo):?>
                <div title="<?php echo $tipoInformativo->descricao;?>">
                    <input type="checkbox" name="informativo[]" value="<?php echo $tipoInformativo->id;?>" <?php echo ($tipoInformativo->checked) ? 'checked="checked"' : '';?> />
                    <span><?php echo $tipoInformativo->nome;?></span>
                </div>
            <?php endforeach;?>
        </div>
        <div>
            <input type="submit" value="Editar">
        </div>
    </form>
<?php else:?>
    <form method="post" class="orcamento no-float" action="<?php echo site_url('admin/empresas/editar/' . $id);?>">
        <?php echo validation_errors();?>
        <input type="hidden" name="id" value="<?php echo $id;?>" />
        <div>
            <label class="span-3">Razão Social</label>
            <input type="text" name="razao_social" class="span-8" value="<?php echo set_value('razao_social');?>" />
        </div>
        <div>
            <label class="span-3">Nome Fantasia</label>
            <input type="text" name="nome_fantasia" class="span-8" value="<?php echo set_value('nome_fantasia');?>" />
        </div>
        <div>
            <label class="span-3">CNPJ</label>
            <input type="text" name="cnpj" class="span-8" value="<?php echo set_value('cnpj');?>" />
        </div>
        <div>
            <label class="span-3">Registro A.N.S.</label>
            <input type="text" name="registro_ans" class="span-8" value="<?php echo set_value('registro_ans');?>" />
        </div>
        <div>
            <label class="span-3">Categoria</label>
            <input type="text" name="categoria" class="span-8" value="<?php echo set_value('categoria');?>" />
        </div>
        <div>
            <label class="span-3">Nome do representante</label>
            <input type="text" name="nome_representante" class="span-8" value="<?php echo set_value('nome_representante');?>" />
        </div>
        <div>
            <label class="span-3">E-mail do representante</label>
            <input type="text" name="email_representante" class="span-8" value="<?php echo set_value('email_representante');?>" />
        </div>
        <div>
            <label class="span-3">Usuário</label>
            <input type="text" name="usuario" class="span-8" value="<?php echo set_value('usuario');?>" />
        </div>
        <div>
            <label class="span-3">Nova Senha (opcional)</label>
            <input type="password" name="senha" class="span-8"  />
        </div>
        <div>
            <label class="span-3">Status</label>
            <select name="status">
                <option value="a" <?php echo set_select('status', 'a');?>>Ativado</option>
                <option value="d" <?php echo set_select('status', 'd');?>>Desativado</option>
                <option value="b" <?php echo set_select('status', 'b');?>>Bloqueado</option>
            </select>
        </div>
        <div>
            <span style="margin-top: 10px;margin-bottom: 10px;display: block;font-weight: bold;">Tipos de informativos adquiridos</span>
            <?php foreach ($tiposInformativos as $tipoInformativo):?>
                <div title="<?php echo $tipoInformativo->descricao;?>">
                    <input type="checkbox" name="informativo[]" value="<?php echo $tipoInformativo->id;?>" <?php echo set_checkbox('informativo', $tipoInformativo->id); ?> />
                    <span><?php echo $tipoInformativo->nome;?></span>
                </div>
            <?php endforeach;?>
        </div>
        <div>
            <input type="submit" value="Editar">
        </div>
    </form>
<?php endif;?>