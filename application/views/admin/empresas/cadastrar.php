<h2>
    <span>Cadastrar empresa</span>
</h2>
<form action="<?php echo site_url(array('admin/empresas', 'cadastrar'));?>" method="post" class="orcamento no-float">
    <?php echo validation_errors();?>
    <div>
        <label class="span-3">Razão Social</label>
        <input type="text" name="razao_social" class="span-8" value="<?php echo set_value('razao_social');?>" />
    </div>
    <div>
        <label class="span-3">Nome Fantasia</label>
        <input type="text" name="nome_fantasia" class="span-8" value="<?php echo set_value('nome_fantasia');?>"  />
    </div>
    <div>
        <label class="span-3">CNPJ</label>
        <input type="text" name="cnpj" class="span-8" value="<?php echo set_value('cnpj');?>"  />
    </div>
    <div>
        <label class="span-3">Registro A.N.S.</label>
        <input type="text" name="registro_ans" class="span-8" value="<?php echo set_value('registro_ans');?>"  />
    </div>
    <div>
        <label class="span-3">Categoria</label>
        <input type="text" name="categoria" class="span-8" value="<?php echo set_value('categoria');?>"  />
    </div>
    <div>
        <label class="span-3">Nome do representante</label>
        <input type="text" name="nome_representante" class="span-8" value="<?php echo set_value('nome_representante');?>"  />
    </div>
    <div>
        <label class="span-3">E-mail do representante</label>
        <input type="text" name="email_representante" class="span-8" value="<?php echo set_value('email_representante');?>"  />
    </div>
    <div>
        <label class="span-3">Usuário</label>
        <input type="text" name="usuario" class="span-8" value="<?php echo set_value('usuario');?>"  />
    </div>
    <div>
        <label class="span-3">Senha</label>
        <input type="password" name="senha" class="span-8" />
    </div>
    <div>
    	<span style="margin-top: 10px;margin-bottom: 10px;display: block;font-weight: bold;">Tipos de informativos adquiridos</span>
    	<?php foreach ($tiposInformativos as $tipoInformativo):?>
            <div title="<?php echo $tipoInformativo->descricao;?>">
                <input type="checkbox" name="informativo[]" value="<?php echo $tipoInformativo->id;?>" <?php echo set_checkbox('informativo', $tipoInformativo->id); ?> />
                <span><?php echo $tipoInformativo->nome;?></span>
            </div>
        <?php endforeach;?>
    </div>
    <div>
        <input type="submit" value="Cadastrar">
    </div>
</form>