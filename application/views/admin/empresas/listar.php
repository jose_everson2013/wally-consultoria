<h2>
    <span>Listagem de Empresas</span>
</h2>
<h3>Edite e altere status de empresas</h3>
<table>
    <thead>
        <tr>
            <th>Razão Social</th>
            <th>Nome Fantasia</th>
            <th>Usuario</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($empresas as $empresa):?>
            <tr>
                <td><?php echo $empresa->razao_social; ?></td>
                <td><?php echo $empresa->nome_fantasia; ?></td>
                <td><?php echo $empresa->usuario; ?></td>
                <td><a href="<?php echo site_url('admin/empresas/editar/' . $empresa->id);?>">Editar</a></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>