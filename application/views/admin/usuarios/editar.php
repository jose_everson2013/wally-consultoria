<?php echo validation_errors();?>
<h2>
    <span>Editando o Admin <?php echo $admin[0]->nome;?></span>
</h2>
<form method="post" class="orcamento no-float" action="<?php echo site_url('admin/usuarios/editar/' .$admin[0]->id);?>">
    <input type="hidden" name="id" value="<?php echo $admin[0]->id;?>" />
    <div>
        <label class="span-3">Nome</label>
        <input type="text" name="nome" class="span-8" value="<?php echo $admin[0]->nome;?>" />
    </div>
    <div>
        <label class="span-3">Nome de usuário</label>
        <input type="text" name="usuario" class="span-8" value="<?php echo $admin[0]->usuario; ?>" />
    </div>
    <div>
        <input type="submit" value="Salvar">
    </div>
</form>
<h2>
    <span>Alterar a Senha</span>
</h2>
<form method="post" class="orcamento no-float" action="<?php echo site_url('admin/usuarios/alterar_senha/' . $admin[0]->id);?>">
    <div>
        <label class="span-3">Nova senha</label>
        <input type="password" name="senha" class="span-8" />
    </div>
    <div>
        <label class="span-3">Confirmar senha</label>
        <input type="password" name="confirmar_senha" class="span-8" />
    </div>
    <div>
        <input type="submit" value="Salvar" />
    </div>
</form>