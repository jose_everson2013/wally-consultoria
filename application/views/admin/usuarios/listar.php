<h2><span>Listagem de Administradores</span></h2>
<h3>Edite e exclua administradores</h3>
<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Usuário</th>
            <th>Editar</th>
            <th>Excluir</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($admins as $admin):?>
            <tr>
                <td><?php echo $admin->id;?></td>
                <td><?php echo $admin->nome; ?></td>
                <td><?php echo $admin->usuario; ?></td>
                <td><a href="<?php echo site_url('admin/usuarios/editar/' . $admin->id);?>">Editar</a></td>
                <td><a href="<?php echo site_url('admin/usuarios/excluir/' . $admin->id);?>">Excluir</a></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>