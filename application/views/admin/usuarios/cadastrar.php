<h2><span>Cadastrar administrador</span></h2>
<form action="<?php echo site_url();?>admin/usuarios/cadastrar" method="post" class="orcamento no-float">
    <?php echo validation_errors();?>
    <div>
        <label class="span-3">Nome</label>
        <input type="text" name="nome" class="span-8" value="<?php echo set_value('nome');?>" />
    </div>
    <div>
        <label class="span-3">Nome de Usuário</label>
        <input type="text" name="usuario" class="span-8" value="<?php echo set_value('usuario');?>" />
    </div>
    <div>
        <label class="span-3">Senha</label>
        <input type="password" name="senha" class="span-8" />
    </div>
    <div>
        <input type="submit" value="Cadastrar">
    </div>
</form>