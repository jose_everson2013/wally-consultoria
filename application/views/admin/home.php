<?php if(isAdmin()):?>
			<ul class="menu span-24 textosVisiveis">
					<li class="titulo"><h2>Gerenciar <br/><span>Empresas</span></h2></li>	
                    <li>
						<a class="umaLinha" href="<?php echo site_url('admin/empresas/cadastrar');?>">

							<span>Cadastrar Empresas</span>
						</a>
					</li>	
                    <li>
						<a class="umaLinha" href="<?php echo site_url('admin/empresas');?>">

							<span>Editar Empresas</span>
						</a>
					</li>
              </ul>
              <ul class="menu span-24 textosVisiveis">   
              		<li class="titulo"><h2>Gerenciar <br/><span>Informativos</span></h2></li>                 
					<li>
						<a class="umaLinha" href="<?php echo site_url('admin/informativos/cadastrar');?>">

							<span>Cadastrar Informativos</span>
						</a>
					</li>	
                    <li>
						<a class="umaLinha" href="<?php echo site_url(array('admin/informativos'));?>">

							<span>Editar Informativos</span>
						</a>
					</li>	
             </ul>
             <ul class="menu span-24 textosVisiveis">  
             		<li class="titulo"><h2>Gerenciar <br/><span>Usuários</span></h2></li>     			
                    <li>
						<a class="umaLinha" href="<?php echo site_url('admin/usuarios/cadastrar');?>">
							<span>Cadastrar Admin</span>
						</a>					
					</li>
                    <li>
						<a class="umaLinha" href="<?php echo site_url('admin/usuarios/listar');?>">
							<span>Editar Admins</span>
						</a>					
					</li>						
				</ul>
            <ul class="menu span-24 textosVisiveis">
                <li class="titulo"><h2>Gerenciar <br/><span>Tipos de informativos</span></h2></li>
                <li>
                    <a class="umaLinha" href="<?php echo site_url(array('admin/tipos_informativos', 'cadastrar'));?>">
                        <span>Cadastrar Tipo de informativos</span>
                    </a>					
                </li>
                <li>
                    <a class="umaLinha" href="<?php echo site_url(array('admin/tipos_informativos'));?>">
                        <span>Editar Tipos de informativos</span>
                    </a>				    
                </li>						
            </ul>
            <ul class="menu span-24 textosVisiveis">
                <li class="titulo"><h2>Gerenciar <br/><span>Grupos de empresas</span></h2></li>
                <li>
                    <a class="umaLinha" href="<?php echo site_url(array('admin/grupos_empresas', 'cadastrar'));?>">
                        <span>Cadastrar Grupo de empresas</span>
                    </a>					
                </li>
                <li>
                    <a class="umaLinha" href="<?php echo site_url(array('admin/grupos_empresas'));?>">
                        <span>Gerenciar Grupos de empresas</span>
                    </a>				    
                </li>						
            </ul>
			<!-- /MENU MEU PORTAL -->
<?php else:?>
			<ul class="menu span-24 textosVisiveis">
					<li class="titulo"><h2>Gerenciar <br/><span>Funcionários</span></h2></li>	
                    <li>
						<a class="umaLinha" href="<?php echo site_url('admin/funcionarios/cadastrar');?>">

							<span>Cadastrar Funcionários</span>
						</a>
					</li>	
                    <li>
						<a class="umaLinha" href="<?php echo site_url('admin/funcionarios/listar');?>">

							<span>Editar Funcionários</span>
						</a>
					</li>
              </ul>
<?php endif ?>