<h2>
    <span>Editar Tipo de informativo</span>
</h2>
<?php if(!empty($tipoInformativo)):?>
    <form action="<?php echo site_url(array('admin/tipos_informativos', 'editar'));?>" method="post" class="orcamento no-float">
        <?php echo validation_errors();?>
        <input name="id" type="hidden" value="<?php echo $tipoInformativo[0]->id;?>" />
        <div>
            <label class="span-3">Nome do informativo</label>
            <input type="text" name="nome" class="span-8" value="<?php echo $tipoInformativo[0]->nome;?>" />
        </div>
        <div>
            <label class="span-3">Descrição do informativo</label>
            <input type="text" name="descricao" class="span-8" value="<?php echo $tipoInformativo[0]->descricao;?>" />
        </div>
        <div>
            <input type="submit" value="Editar">
        </div>
    </form>
<?php else:?>
        <form action="<?php echo site_url(array('admin/tipos_informativos', 'editar'));?>" method="post" class="orcamento no-float">
            <?php echo validation_errors();?>
            <input name="id" type="hidden" value="<?php echo set_value('id');?>" />
            <div>
                <label class="span-3">Nome do informativo</label>
                <input type="text" name="nome" class="span-8" value="<?php echo set_value('nome');?>" />
            </div>
            <div>
                <label class="span-3">Descrição do informativo</label>
                <input type="text" name="descricao" class="span-8" value="<?php echo set_value('descricao');?>" />
            </div>
            <div>
                <input type="submit" value="Editar">
            </div>
        </form>
<?php endif;?>