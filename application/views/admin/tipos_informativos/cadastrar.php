<h2>
    <span>Cadastrar Tipo de informativo</span>
</h2>
<form action="<?php echo site_url(array('admin/tipos_informativos', 'cadastrar'));?>" method="post" class="orcamento no-float">
    <?php echo validation_errors();?>
    <div>
        <label class="span-3">Nome do informativo</label>
        <input type="text" name="nome" class="span-8"  />
    </div>
    <div>
        <label class="span-3">Descrição do informativo</label>
        <input type="text" name="descricao" class="span-8"  />
    </div>
    <div>
        <input type="submit" value="Cadastrar">
    </div>
</form>