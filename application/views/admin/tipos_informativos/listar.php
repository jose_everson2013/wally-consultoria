<h2>
    <span>Listagem de Tipos de informativos</span>
</h2>
<h3>Edite os tipos de informativos</h3>
<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Descrição</th>
            <th>Editar</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($tiposInformativos as $tipoInformativo):?>
            <tr>
                <td><?php echo $tipoInformativo->id; ?></td>
                <td><?php echo $tipoInformativo->nome; ?></td>
                <td><?php echo $tipoInformativo->descricao; ?></td>
                <td><a href="<?php echo site_url(array('admin/tipos_informativos/', 'editar', $tipoInformativo->id));?>">Editar</a></td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>