<h2>
    <span>Cadastrar funcionário</span>
</h2>
<form action="<?php echo site_url('admin/funcionarios/cadastrar');?>" method="post" class="orcamento no-float">
    <?php echo validation_errors();?>
    <input type="hidden" name="func[empresa_id]" class="span-8" value="<?php echo getId();?>"  />
    <div>
        <label class="span-3">Nome</label>
        <input type="text" name="func[nome]" value="<?php echo set_value('func[nome]', ''); ?>" class="span-8"  />
    </div>
    <div>
        <label class="span-3">Email</label>
        <input type="text" name="func[email]" value="<?php echo set_value('func[email]', ''); ?>" class="span-8"  />
    </div>
    <div>
    	<span style="margin-top: 10px;margin-bottom: 10px;display: block;font-weight: bold;">Tipo de informativos que irá receber</span>
        <?php foreach ($tiposInfos as $tipoInfo):?>
            <div title="<?php echo $tipoInfo->descricao;?>">
                <input type="checkbox" name="informativo[]" value="<?php echo $tipoInfo->id; ?>" <?php echo set_checkbox('informativo', $tipoInfo->id);?> />
                <span><?php echo $tipoInfo->nome;?></span>
            </div>
        <?php endforeach ?>
    </div>
    <br />
    <div>
        <input type="submit" value="Cadastrar">
    </div>
</form>