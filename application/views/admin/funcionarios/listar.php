<h2>
    <span>Listagem de Funcionários</span>
</h2>
<h3>Edite funcionários</h3>
<table>
    <thead>
        <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Editar</th>
            <th>Excluir</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($funcionarios as $funcionario):?>
            <tr>
                <td><?php echo $funcionario->id; ?></td>
                <td><?php echo $funcionario->nome; ?></td>
                <td><?php echo $funcionario->email; ?></td>
                <td><a href="<?php echo site_url('admin/funcionarios/editar/' . $funcionario->id);?>">Editar</a></td>
                <td>
                    <a href="<?php echo site_url('admin/funcionarios/excluir/' . $funcionario->id);?>" onclick="return confirm('Deseja realmente excluir o funcionário #<?php echo $funcionario->id;?>?');">
                        Excluir
                    </a>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>