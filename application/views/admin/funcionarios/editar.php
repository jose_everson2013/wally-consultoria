<h2>
    <span>Editando Funcionário</span>
</h2>
<form method="post" class="orcamento no-float" action="<?php echo site_url('admin/funcionarios/editar/' . $funcionario[0]->id);?>">
    <?php echo validation_errors();?>
    <input type="hidden" name="func[id]" value="<?php echo $funcionario[0]->id;?>" />
    <div>
        <label class="span-3">Nome</label>
        <input type="text" name="func[nome]" class="span-8" value="<?php echo $funcionario[0]->nome;?>" />
    </div>
    <div>
        <label class="span-3">Email</label>
        <input type="text" name="func[email]" class="span-8" value="<?php echo $funcionario[0]->email;?>" />
    </div>
    <div>
    	<span style="margin-top: 10px;margin-bottom: 10px;display: block;font-weight: bold;">Tipo de informativos que irá receber</span>
    	<?php foreach ($tiposInfos as $tipoInfo):?>
            <div title="<?php echo $tipoInfo->descricao;?>">
                <input type="checkbox" name="informativo[]" value="<?php echo $tipoInfo->id; ?>" <?php echo ($tipoInfo->checked) ? 'checked' : '' ; ?> />
                <span><?php echo $tipoInfo->nome;?></span>
            </div>
        <?php endforeach ?>
    </div>
    <br /><br />
    <div>
        <input type="submit" value="Editar">
    </div>
</form>