<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Área administrativa - Wally Consultoria</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo url_css('favicon.ico');?>">
    <link rel="stylesheet" href="<?php echo site_url();?>assets/css/blueprint/screen.css" type="text/css" media="screen, projection"/>
    <link rel="stylesheet" href="<?php echo site_url();?>assets/css/blueprint/print.css" type="text/css" media="print"/>
    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo site_url();?>assets/css/blueprint/ie.css" type="text/css" media="screen, projection"/><![endif]-->
    <link href="<?php echo url_css('default.css');?>" rel="stylesheet" type="text/css"/>
    <link href="<?php echo url_css('smoothness/jquery-ui.css');?>" rel="stylesheet" type="text/css"/>
    
    <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-1.9.1.min.js');?>"></script>
    <script type="text/javascript" src="<?php echo url_js('jquery-ui.js');?>"></script>
    <!-- TinyMCE -->
    <script type="text/javascript" src="<?php echo site_url();?>assets/js/tinymce_pt/jscripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="<?php echo site_url();?>assets/js/tinymce_pt/jscripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="<?php echo site_url();?>assets/js/tinymce_pt/jscripts/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options
            relative_urls : false,
            remove_script_host: false,
            language : "pt",
            mode : "textareas",
            theme : "advanced",
            plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

            // Theme options
            theme_advanced_buttons1:
                "code,bold,italic,underline,strikethrough,justifyleft,justifycenter,justifyright,justifyfull,cleanup,link,unlink,image,table,formatselect,fontselect,fontsizeselect,forecolor,backcolor,fullscreen",

            // Theme options
            theme_advanced_buttons2 : "",
            theme_advanced_buttons3 : "",
            theme_advanced_buttons4 : "",

            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            // Example content CSS (should be your site CSS)
            content_css : "css/content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js",
            media_external_list_url : "lists/media_list.js",
            file_browser_callback : "tinyBrowser",

            // Replace values for the template plugin
            template_replace_values : {
                username : "Some User",
                staffid : "991234"
            }
        });
    </script>
    <!-- /TinyMCE -->
    
</head>
<body <?php /*echo (isAdvertiser() || isAdmin()) ? 'id="index" class="logado"' : 'id="login"';*/?>>
    <!-- TOPO -->
    <div id="topo">
        <?php /*if(isAdvertiser() || isAdmin()):*/?>
            <div class="barraLogado">
                <div class="container">
                    <span>Olá <?php echo getUserName();?></span>
                    <span class="logout">
                        <a class="" href="<?php echo site_url('admin/home/logout');?>">Deslogar</a>   
                    </span>
                </div>
            </div>
        <?php //endif;?>
        <div class="container">
            <a href="<?php echo site_url();?>admin/home" class="logo"></a>
                <h1 style="color: white;">Área do Gerente</h1>
        </div>
    </div>
    <!-- /TOPO -->
    <!-- CORPO -->
    <div id="corpo">
        <div class="container">
            <?php if(getFlashdata('msg')):?>
                <div class="clear">
                    <p><h2><?php echo getFlashdata('msg');?></h2></p>
                </div>
            <?php endif;?>