<form method="post" action="<?php echo site_url(array('admin/home', 'recuperar_senha'));?>">
    <h2><span>Recuperar senha</span></h2>
    <?php echo validation_errors();?>
    <div>
        <label class="span-3">Insira seu nome de usuário</label>
        <input type="text" name="usuario" class="span-9" value="<?php echo set_value('usuario'); ?>" required autofocus />
    </div>
    <br/>
    <div>
        <label class="span-3">Insira seu e-mail</label>
        <input type="email" name="email" class="span-9" value="<?php echo set_value('email'); ?>" required />
    </div>
    <br/>
    <div>
        <input type="submit" value="Enviar"/>
    </div>
</form>