<body id="home">
    <div id="container">
        <div id="top">
            <div id="logo"><a href="<?php echo site_url(); ?>"><img src="<?php echo url_img('logo.png'); ?>" alt="Wally Consultoria" /></a></div>
            <div id="text_top">Expertise e<br /><span>Conhecimento</span></div>
            <div class="clear"></div>
        </div>
        <?php echo $menu;?>
        <div id="face"><a href=""><img src="<?php echo url_img('face.png'); ?>" alt="Facebook" /></a></div>
        <div id="phone"><span>11</span> 2847.4553</div>
        <div class="clear"></div>
        <div id="restricted">
            Área Restrita
            <form method="post" action="<?php echo site_url(array('admin/home', 'login'));?>">
                <input type="text" name="usuario" id="login" placeholder="Seu login" />
                <input type="password" name="senha" id="senha" placeholder="Sua senha" />
                <input type="image" src="<?php echo url_img('btn.png'); ?>" id="button_login" alt="Logar" />
                <a href="<?php echo site_url(array('admin/home', 'recuperar_senha'));?>">Esqueci minha senha.</a>
            </form>
            
        </div>
        <div id="banner">
            <a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" alt="" /></a>
            <a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" alt="" /></a>
        </div>
        <div id="boxes">
            <div id="quem">
                <h2>Quem Somos</h2>
                <img src="<?php echo url_img('hands.jpg'); ?>" alt="" />
                <p>Somos uma empresa especializada na prestação de serviços de Assessoria e Consultoria Técnica e Atuarial, focada no segmento de Operadoras de Planos de Assistência à Saúde.</p>
                <a href="<?php echo site_url('quem_somos'); ?>">Leia mais...</a>
            </div>
            <div id="info">
                <h2>Informativos</h2>
                <a href="<?php echo site_url('informativos'); ?>"><img src="<?php echo url_img('info.jpg'); ?>" alt="" /></a>
                <ul id="fade1" class="list-ticker">
                    <?php if(!empty($exibe)):?>
                        <?php foreach ($exibe as $item):?>
                            <li>
                                <p>
                                    <a href="<?php echo site_url('informativos'); ?>">
                                        <?php echo $item->titulo; ?>
                                    </a>
                                </p>
                            </li>
                        <?php endforeach;?>
                        <li>
                            <p>Assessoria e Consultoria Técnica e Atuarial, focada no segmento: Assistência à Saúde.</p>
                        </li>
                    <?php else:?>
                        <li>
                            <p>Assessoria e Consultoria Técnica e Atuarial, focada no segmento: Assistência à Saúde.</p>
                        </li>
                    <?php endif;?>
                </ul>
            </div>
            <div id="news">
                <h2>News</h2>
                <img src="<?php echo url_img('news.jpg'); ?>" alt="" />
                <ul id="fade2" class="list-ticker">
                    <?php if(!empty($informativos)):?>
                        <?php foreach ($informativos as $item):?>
                            <li>
                                <p>
                                    <a href="<?php echo $item->link; ?>" target="_blank">
                                        <?php echo $item->title; ?>
                                        <?php echo $item->description; ?>
                                    </a>
                                </p>
                            </li>
                        <?php endforeach;?>
                    <?php else:?>
                        <li>
                            <p>Assessoria e Consultoria Técnica e Atuarial, focada no segmento: Assistência à Saúde.</p>
                        </li>
                    <?php endif;?>
                </ul>
            </div>
        </div>
        <div id="footer" style="margin-top: 99px;">
            <div id="address">
                    <b>Av. Paulista, 2300 – Andar Pilotis – Bela Vista</b>
                            CEP: 01310-300 - São Paulo - SP
                    </div>
            <div id="copyright">
                <span><b>Wally Consultoria -</b> 2013 - Todos os direitos reservados</span>
            </div>
            <div id="development">
                <a href="http://www.kmcnigro.com.br" target="_blank"><img src="<?php echo url_img('kmcnigro.png'); ?>" alt="KMC Nigro" /></a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#fade1, #fade2').list_ticker({
                speed: 5000,
                effect:'fade'
            });
        });
    </script>
