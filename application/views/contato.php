<body>

<div id="container">
	<div id="top">
    	<div id="logo"><a href="<?php echo site_url(); ?>"><img src="<?php echo url_img('logo.png'); ?>" alt="Wally Consultoria" /></a></div>
        <div id="text_top">Expertise e<br /><span>Conhecimento</span></div>
        <div class="clear"></div>
    </div>
    
    <?php echo $menu;?>
    
    <div id="face"><a href=""><img src="<?php echo url_img('face.png'); ?>" alt="Facebook" /></a></div>
    <div id="phone"><span>11</span> 2847.4553</div>
    <div class="clear"></div>
    <div id="restricted">Área Restrita
    	<form method="post" action="<?php echo site_url('home/login');?>">
            <input type="text" name="usuario" id="login" placeholder="Seu login" />
            <input type="password" name="senha" id="senha" placeholder="Sua senha" />
            <input type="image" src="<?php echo url_img('btn.png'); ?>" id="button_login" alt="Login" />             
            <a href="<?php echo site_url(array('admin/home', 'recuperar_senha'));?>">Esqueci minha senha.</a>
        </form>
    </div>
    <div id="banner">
    	<a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" alt="" /></a>
        <a href=""><img src="<?php echo url_img('banner/banner1.png'); ?>" alt="" /></a>
    </div>
    <div id="content_top">
    	<span>Contato<br />
            <span id="ddd">11</span>
            <span id="telefone">2847.4553</span>
        </span>
        <div id="content_text">Entre em contato com a Wally através do nosso formulario de atendimento. <b>Retornaremos o mais breve possível!</b></div>
        <?php echo validation_errors(); ?>
    </div>
    <div id="form">
    	<form method="post" action="<?php echo site_url('home/enviar');?>">
        	<label id="name">Nome</label><input type="text" name="nome" />
            <label id="emp"><span>* </span>Empresa</label><input type="text" name="empresa" /><br />
            <label id="email">E-mail</label><input type="text" name="email" /><br />
            <label id="telephone"><span>* </span>Telefone</label><input type="text" name="telephone" /><br />
            <label id="message">Mensagem</label><textarea name="message"></textarea>
            <input type="image" src="<?php echo url_img('btn_form.png'); ?>" alt="Enviar" />
            
        </form>
        <span class="fields">* Campos não obrigatórios</span>
    </div>
    <div class="clear"></div>
    <div id="footer" style="margin-top: 99px;">
    	<div id="address">
        	<b>Av. Paulista, 2300 – Andar Pilotis – Bela Vista</b>
			CEP: 01310-300 - São Paulo - SP
		</div>
        <div id="copyright">
        	<span><b>Wally Consultoria -</b> 2013 - Todos os direitos reservados</span>
        </div>
        <div id="development">
        	<a href="http://www.kmcnigro.com.br" target="_blank"><img src="<?php echo url_img('kmcnigro.png'); ?>" alt="KMC Nigro" /></a>
        </div>
        <div class="clear"></div>
    </div>
</div>
