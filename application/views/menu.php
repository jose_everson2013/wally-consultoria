<div id="menu">
    <ul>
        <li <?php echo isActivePage('home');?>><a href="<?php echo site_url(); ?>">Home</a></li>
        <li <?php echo isActivePage('quem_somos');?>><a href="<?php echo site_url('quem_somos'); ?>">Quem Somos</a></li>
        <li <?php echo isActivePage('servicos');?>><a href="<?php echo site_url('servicos'); ?>">Serviços</a></li>
        <li <?php echo isActivePage('informativos');?>><a href="<?php echo site_url('informativos'); ?>">Informativos</a></li>
        <li <?php echo isActivePage('contato');?>><a href="<?php echo site_url('contato'); ?>">Contato</a></li>
	</ul>
</div>