<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Wally Consultoria</title>
    <link rel="stylesheet" type="text/css" href="<?php echo url_css('style.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo url_css('list-ticker.css'); ?>" />
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo url_css('styleie8.css'); ?>" />
    <![endif]-->
    <script src="<?php echo url_js('jquery-1.9.1.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo url_js('jquery.cycle.all.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo url_js('jquery.placeholder.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo url_js('list-ticker.js'); ?>" type="text/javascript"></script>
    <script type="text/javascript">
    $(function() {
            $('input, textarea').placeholder();
    });
    </script>
</head>