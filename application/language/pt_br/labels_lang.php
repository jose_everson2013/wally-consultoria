<?php
$lang["_home"] = "Home";
$lang["_about_us"] = "Quem Somos";
$lang["_partners"] = "Parceiros";
$lang["_advertise_here"] = "Anuncie Aqui";
$lang["_contact"] = "Contato";
$lang["_advanced_search"] = "Pesquisa avançada";
$lang["_ombudsman"] = "Ouvidoria";
$lang["_suggestions"] = "Sugestões";
$lang["_faq"] = "Dúvidas Frequentes";
$lang["_welcome"] = "Seja bem vindo";
$lang["_iam"] = "eu sou";
$lang["_my_profile"] = "Meu Perfil";
$lang["_ethnicity"] = "Etnia";
$lang["_hair_color"] = "Cor do cabelo";
$lang["_eyes_color"] = "Cor dos olhos";
$lang["_age"] = "Idade";
$lang["_height"] = "Altura";
$lang["_weight"] = "Peso";
$lang["_mannequin"] = "Manequim";
$lang["_who_attends"] = "Quem atende";
$lang["_languages"] = "Idiomas";
$lang["_where_to_meet"] = "Onde atende";
$lang["_available_days"] = "Dias disponíveis";
$lang["_available_timetable"] = "Horários disponíveis";
$lang["_city"] = "Cidade";
$lang["_neighborhood"] = "Bairro";
$lang["_my_contacts"] = "Meus contatos";
$lang["_send_me_an_email"] = "Entre em contato por e-mail";
$lang["_name"] = "Nome";
$lang["_email"] = "Email";
$lang["_telephone"] = "Telefone";
$lang["_email_body"] = "Corpo do email";
$lang["_leave_a_comment_about_me"] = "Deixe um comentário sobre mim";
$lang["_comment"] = "Comentário";
$lang["_send"] = "Enviar";
$lang["_accompanying_best_evaluated_this_month"] = "As acompanhantes mais avaliadas do m?s";
$lang["_fill_out_the_form_below"] = "Preencha o formulário abaixo:";
$lang["_full_name"] = "Nome completo";
$lang["_confirm_email"] = "Confirmar email";
$lang["_user_name"] = "Nome de usuário";
$lang["_password"] = "Senha";
$lang["_confirm_password"] = "Confirmar senha";
$lang["_register"] = "Cadastrar";
$lang["_select"] = "Selecione";
$lang["_subject"] = "Assunto";
$lang["_message"] = "Mensagem";
$lang["_set_up_your_profile"] = "Monte seu perfil";
$lang["_yes"] = "Sim";
$lang["_no"] = "Não";
$lang["_search"] = "Pesquisar";
$lang["_the_fields_with_*_are_required"] = "Os campos com * são obrigatórios";
$lang["_all_right_reserved"] = "Todos os direitos reservados";
$lang["_development"] = "Desenvolvimento";
$lang["_localization"] = "Localização";
$lang["_comment_successfully_sent"] = "Comentário enviado com sucesso. Aguardando aprovação para publicação.";
$lang["_email_successfully_sent"] = "E-mail enviado com sucesso.";
?>