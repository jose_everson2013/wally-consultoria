<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Func_tipo_informativo_model extends CI_Model {
	public $table = 'func_tipo_informativo';
	
	public function __construct(){
        parent::__construct();
    }
	
	public function register($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
	
	public function getTypesByEmployeeId($employId){
		$this->db->where('funcionario_id', $employId);
		return $this->db->get($this->table)->result();
	}
	
	public function getEmployeesByType($type){
		$this->db->select('func.*, e.email_representante');
		$this->db->join('funcionarios as func', 'func.id = fti.funcionario_id');
                $this->db->join('empresas AS e', 'e.id = func.empresa_id');
		$this->db->where('fti.tipo_informativo_id', $type);
                $this->db->where('e.status', 'a');
		return $this->db->get('func_tipo_informativo AS fti')->result();
	}
	
	public function deleteTypesByEmployeeId($employId){
		$this->db->where('funcionario_id', $employId);
		return $this->db->delete($this->table);
	}
}