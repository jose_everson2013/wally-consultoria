<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Informativos_model extends CI_Model {

    public $table = 'informativos';

    public function __construct(){
        parent::__construct();
    }

    public function select_informativos(){
        $this->db->order_by('id', 'DESC');
        return $this->db->get('informativos')->result();
    }

    public function getInfomationById($id){
        $this->db->where('id', $id);
        return $this->db->get($this->table)->result();
    }
	
    public function register($data){
        $data['data_cadastro'] = date('Y/m/d h:i:s');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
	
	public function update($data){
        $this->db->where('id', $data['id']);
        return $this->db->update($this->table, $data);
    }
	
	public function delete($id){
        $this->db->where('id', $id);
        return $this->db->delete('informativos');
    }
    
    public function getInformationForSite() {
        $this->db->where('status', 's');
        $this->db->order_by('data_cadastro', 'DESC');
        return $this->db->get($this->table)->result();
    }
	
}