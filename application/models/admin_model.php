<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    public function login($usuario, $senha){
        $this->db->select('id, usuario');
        $this->db->where('usuario', $usuario);
        $this->db->where('senha', sha1($senha));
        return $this->db->get('admins')->result();
    }
	 
    public function register($data){
        $this->db->set('nome', $data->nome);
        $this->db->set('usuario', $data->usuario);
        $this->db->set('senha', sha1($data->senha));
        $this->db->insert('admins');
        return $this->db->insert_id();
    }
    
    public function getAdmins(){
        $this->db->select('id, nome, usuario');
        return $this->db->get('admins')->result();
    }
    
    public function getAdmin($id){
        $this->db->select('id, nome, usuario');
        $this->db->where('id', $id);
        return $this->db->get('admins')->result();
    }
    
    public function saveChanges($data){
        $this->db->set('nome', $data->nome);
        $this->db->set('usuario', $data->usuario);
        $this->db->where('id', $data->id);
        return $this->db->update('admins');
    }
    
    public function getUserNameById($id){
        $this->db->select('usuario');
        $this->db->where('id', $id);
        return $this->db->get('admins')->result();
    }
    
    public function alterPassword($userId, $novaSenha){
        $this->db->set('senha', sha1($novaSenha));
        $this->db->where('id', $userId);
        return $this->db->update('admins');
    }
    
    public function delete($id){
        $this->db->where('id', $id);
        return $this->db->delete('admins');
    }
}

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */