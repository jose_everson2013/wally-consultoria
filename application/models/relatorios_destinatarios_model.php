<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relatorios_destinatarios_model extends CI_Model {

    public $table = 'relatorios_destinatarios';

    public function __construct(){
        parent::__construct();
    }

    public function register($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function getRecipientsByReportId($reportId){
    	$this->db->where('relatorio_envio_id', $reportId);
    	return $this->db->get($this->table)->result();
    }
}