<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresas_model extends CI_Model {
    
        public $table = 'empresas';
        
        public function __construct(){
            parent::__construct();
        }
	
	public function login($usuario, $senha){
            $this->db->select('id, usuario');
            $this->db->where('usuario', $usuario);
            $this->db->where('senha', sha1($senha));
            $this->db->where('status', 'a');
            return $this->db->get($this->table)->result();
        }
	
	public function select_empresas(){
            return $this->db->get($this->table)->result();
	}
	
        public function getCompanyById($id) {
            $this->db->where('id', $id);
            return $this->db->get($this->table)->result();
        }
        public function register($data){
            $this->db->insert($this->table, $data);
            return $this->db->insert_id();
        }
	
	public function update($data){
            $this->db->where('id', $data['id']);
            return $this->db->update($this->table, $data);
        }
	
	public function delete($id){
            $this->db->where('id', $id);
            return $this->db->delete('informativos');
        }
        
        public function getCompanyByUsernameAndEmail($data) {
            $this->db->where('email_representante', $data['email']);
            $this->db->where('usuario', $data['usuario']);
            return $this->db->get($this->table)->result();
        }
        
        public function validateHash($hash) {
            $this->db->where('SHA1(CONCAT(usuario, email_representante, senha, redefinicao_senha)) = ', $hash);
            return $this->db->get($this->table)->result();
        }
	
}