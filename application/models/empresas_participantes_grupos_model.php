<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresas_participantes_grupos_model extends CI_Model {
    
    public $table = 'empresas_participantes_grupos';

    public function __construct(){
        parent::__construct();
    }

    public function register($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function delete($data) {
        $this->db->where('grupo_id', $data['grupo_id']);
        return $this->db->delete($this->table);
    }
    
    public function getCompaniesParticipantsByGroupId($groupId) {
        $this->db->where('grupo_id', $groupId);
        return $this->db->get($this->table)->result();
    }
}