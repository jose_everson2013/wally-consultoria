<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresa_tipo_informativo_model extends CI_Model {
	
    public $table = 'empresa_tipo_informativo';
	
    public function __construct(){
        parent::__construct();
    }
	
    public function register($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function getTypesByCompanyId($id) {
        $this->db->where('empresa_id', $id);
        return $this->db->get($this->table)->result();
    }
    
    public function deleteTypesByCompanyId($id) {
        $this->db->where('empresa_id', $id);
        return $this->db->delete($this->table);
    }
}