<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grupos_empresas_model extends CI_Model {
    
    public $table = 'grupos_empresas';

    public function __construct(){
        parent::__construct();
    }

    public function register($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function getCompaniesGroups() {
        return $this->db->get($this->table)->result();
    }
    
    public function getCompanyGroupById($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->table)->result();
    }
    
    public function update($data) {
        $this->db->where('id', $data['id']);
        return $this->db->update($this->table, $data);
    }
    
    public function getCompaniesByGroupId($groupId) {
        $this->db->join('empresas e', 'epg.empresa_id = e.id');
        $this->db->where('e.status', 'a');
        $this->db->where('epg.grupo_id', $groupId);
        return $this->db->get('empresas_participantes_grupos epg')->result();
    }
}