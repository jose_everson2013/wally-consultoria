<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relatorios_envios_model extends CI_Model {

    public $table = 'relatorios_envios';

    public function __construct(){
        parent::__construct();
    }

    public function register($data){
        $data['data_envio'] = date('Y/m/d h:i:s');
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function getReportByInformationId($informationId){
    	$this->db->where('informativo_id', $informationId);
    	return $this->db->get($this->table)->result();
    }
}