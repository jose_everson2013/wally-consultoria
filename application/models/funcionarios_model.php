<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Funcionarios_model extends CI_Model {
	public $table = 'funcionarios';
	
	public function __construct(){
        parent::__construct();
    }
	
	public function select_funcionarios($id = null) {
            if (!empty($id)) {
                $this->db->where('empresa_id', $id);
            }
            return $this->db->get('funcionarios')->result();
	}
	
    public function register($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
	
    public function update($data){
        $this->db->where('id', $data['id']);
        return $this->db->update($this->table, $data);
    }
	
    public function delete($id){
        $this->db->where('id', $id);
        return $this->db->delete('funcionarios');
    }
	
    public function getEmployeeById($data) {
        $this->db->where('id', $data['funcionario_id']);
        $this->db->where('empresa_id', $data['empresa_id']);
        return $this->db->get($this->table)->result();	
    }
    
    public function getEmploeesByTypeIdAndCompanyId($data) {
        $this->db->join('func_tipo_informativo AS fti', 'fti.funcionario_id = f.id');
        $this->db->where('f.empresa_id', $data['empresa_id']);
        $this->db->where('fti.tipo_informativo_id', $data['tipo_informativo_id']);
        return $this->db->get('funcionarios AS f')->result();
    }
}