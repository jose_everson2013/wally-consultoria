<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipos_informativos_model extends CI_Model {

    public $table = 'tipos_informativos';
	
    public function __construct(){
        parent::__construct();
    }
	
    public function register($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    public function getTypesInformation() {
        return $this->db->get($this->table)->result();
    }
    
    public function getTypesInformationById($id) {
        $this->db->where('id', $id);
        return $this->db->get($this->table)->result();
    }

    public function getTypesInformationByCompanyId($id) {
        $this->db->select('ti.*');
        $this->db->join('empresa_tipo_informativo AS eti', 'eti.tipo_informativo_id = ti.id');
        $this->db->where('eti.empresa_id', $id);
        return $this->db->get('tipos_informativos AS ti')->result();
    }
    
    public function update($data){
        $this->db->where('id', $data['id']);
        return $this->db->update($this->table, $data);
    }
}