<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Grupos_empresas extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation', '', 'validacao');
        $this->load->model('grupos_empresas_model', 'grupoEmpresa');
        restrictArea();
    }
    
    public function index(){
        $data['gruposEmpresas'] = $this->grupoEmpresa->getCompaniesGroups();
        $this->load->view('admin/header', $data);
        $this->load->view('admin/grupos_empresas/listar');
        $this->load->view('admin/footer');
    }
    
    public function cadastrar(){
        if($this->input->post() && $this->validar_cadastro()){
            $data = $this->input->post();
            $id = $this->grupoEmpresa->register($data);
            if($id){
                $this->session->set_flashdata('msg', 'Cadastro efetuado com sucesso!');
                redirect('admin/grupos_empresas');
            }else{
                $this->session->set_flashdata('msg', 'Ocorreu um erro. Tente novamente!');
                redirect('admin/grupos_empresas');
            }
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/grupos_empresas/cadastrar');
            $this->load->view('admin/footer');
        }
    }
    
    protected function validar_cadastro() {
        $this->validacao->set_rules('id', 'Id', '');
        $this->validacao->set_rules('nome', 'Nome', 'required');
        return $this->validacao->run();
    }
    
    public function editar($companyGroupId = null){
        if($this->input->post() && $this->validar_cadastro()){
            $data = $this->input->post();
            if($this->grupoEmpresa->update($data)){
                $this->session->set_flashdata('msg', 'Cadastro alterado com sucesso!');
                redirect('admin/grupos_empresas');
            }else{
                $this->session->set_flashdata('msg', 'Ocorreu um erro. Tente novamente!');
                redirect('admin/grupos_empresas');
            }
        }else{
            $data['grupoEmpresa'] = $this->grupoEmpresa->getCompanyGroupById($companyGroupId);
            $this->load->view('admin/header', $data);
            $this->load->view('admin/grupos_empresas/editar');
            $this->load->view('admin/footer');
        }
    }
    
    public function adicionar_empresas($companyGroupId = null) {
        $this->load->model('empresas_participantes_grupos_model', 'empresaPartGrupo');
        if($companyGroupId){
            $this->load->model('empresas_model', 'empresa');
            $data['empresas'] = $this->empresa->select_empresas();
            $data['empresasPartGrupo'] = $this->empresaPartGrupo->getCompaniesParticipantsByGroupId($companyGroupId);
            foreach($data['empresas'] as $index => $empresa){
                $data['empresas'][$index]->checked = false;
                foreach ($data['empresasPartGrupo'] as $empresaPartGrupo) {
                    if($empresaPartGrupo->empresa_id == $data['empresas'][$index]->id){
                        $data['empresas'][$index]->checked = true;
                    }
                }
            }
            $data['grupoEmpresa'] = $this->grupoEmpresa->getCompanyGroupById($companyGroupId);
            $this->load->view('admin/header', $data);
            $this->load->view('admin/grupos_empresas/adicionar_empresas');
            $this->load->view('admin/footer');
        }elseif($this->input->post()){
            $this->validacao->set_rules('empresa_id', 'Empresas', 'required');
            $this->validacao->set_rules('grupo_id', 'Grupo', '');
            $companyGroupId = $this->input->post('grupo_id');
            if(!$this->validacao->run()){
                $this->load->model('empresas_model', 'empresa');
                $data['empresas'] = $this->empresa->select_empresas();
                $data['grupoEmpresa'] = $this->grupoEmpresa->getCompanyGroupById($companyGroupId);
                $this->load->view('admin/header', $data);
                $this->load->view('admin/grupos_empresas/adicionar_empresas');
                $this->load->view('admin/footer');
            }else{
                $empresas = $this->input->post('empresa_id');
                $data['grupo_id'] = $this->input->post('grupo_id');
                $this->empresaPartGrupo->delete($data);
                foreach($empresas as $empresa_id){
                    $data['empresa_id'] = $empresa_id;
                    $this->empresaPartGrupo->register($data);
                }
                redirect('admin/grupos_empresas');
            }
        }else{
            redirect('admin/grupos_empresas');
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */