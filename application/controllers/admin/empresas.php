<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Empresas extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('empresas_model', 'empresa');
        $this->load->model('tipos_informativos_model', 'tipoInformativo');
        $this->load->model('empresa_tipo_informativo_model', 'empresaTipo');
        $this->load->library('form_validation', '', 'validacao');
    }

    public function cadastrar(){
        restrictArea();
        if($this->validar_cadastro()){
            $data = $this->input->post();
            $data['senha'] = sha1($data['senha']);
            unset($data['informativo']);
            $id = $this->empresa->register($data);
            if(!empty($id)){
                foreach ($this->input->post('informativo') as $informativo){
                    $empresaInformativo['empresa_id'] = $id;
                    $empresaInformativo['tipo_informativo_id'] = $informativo;
                    $this->empresaTipo->register($empresaInformativo);
                }
                $this->session->set_flashdata('msg', 'Cadastro efetuado com sucesso.');
                redirect('admin/home');
            }else{
                $this->session->set_flashdata('msg', 'Erro ao acessar o banco de dados.');
                redirect('admin/home');
            }
        }else{
            $data['tiposInformativos'] = $this->tipoInformativo->getTypesInformation();
            $this->load->view('admin/header', $data);
            $this->load->view('admin/empresas/cadastrar');
            $this->load->view('admin/footer');
        }
    }

    public function validar_cadastro(){
        $this->validacao->set_rules('razao_social', 'Razão Social', 'required');
        $this->validacao->set_rules('nome_fantasia', 'Nome Fantasia', 'required');
        $this->validacao->set_rules('cnpj', 'CNPJ', 'required');
        $this->validacao->set_rules('registro_ans', 'Registro A.N.S.', 'required');
        $this->validacao->set_rules('categoria', 'Categoria', 'required');
        $this->validacao->set_rules('nome_representante', 'Nome do representante', 'required');
        $this->validacao->set_rules('email_representante', 'E-mail do representante', 'required');
        $this->validacao->set_rules('usuario', 'Usuário', 'required|is_unique[empresas.usuario]');
        $this->validacao->set_rules('senha', 'Senha', 'required');
        $this->validacao->set_rules('informativo', 'Informativo', 'required');
        return $this->validacao->run();
    }
	
	public function index(){
        restrictArea();
        $data['empresas'] = $this->empresa->select_empresas();
        $this->load->view('admin/header');
        $this->load->view('admin/empresas/listar', $data);
        $this->load->view('admin/footer');
    }
	
	public function editar($id){
        restrictArea();
        $data['tiposInformativos'] = $this->tipoInformativo->getTypesInformation();
        if(!$this->input->post()){
            $data['empresa'] = $this->empresa->getCompanyById($id);
            $data['empresa']['tiposInformativos'] = $this->empresaTipo->getTypesByCompanyId($id);
            foreach ($data['tiposInformativos'] as $index => $tipoInformativo) {
                $data['tiposInformativos'][$index]->checked = false;
                foreach ($data['empresa']['tiposInformativos'] as $empresaTipoInformativo) {
                    if($tipoInformativo->id == $empresaTipoInformativo->tipo_informativo_id){
                        $data['tiposInformativos'][$index]->checked = true;
                    }
                }
            }
            $this->load->view('admin/header');
            $this->load->view('admin/empresas/editar', $data);
            $this->load->view('admin/footer');
        }else{
            if($this->validar_alteracao()){
                $data = $this->input->post();
                if(!empty($data['senha'])) 
                    $data['senha'] = sha1($data['senha']);
                else
                    unset($data['senha']);
                unset($data['informativo']);
                $this->empresaTipo->deleteTypesByCompanyId($data['id']);
                foreach ($this->input->post('informativo') as $informativo){
                    $empresaInformativo['empresa_id'] = $id;
                    $empresaInformativo['tipo_informativo_id'] = $informativo;
                    $this->empresaTipo->register($empresaInformativo);
                }
                $this->empresa->update($data);
                $this->session->set_flashdata('msg', 'Empresa atualizada com sucesso.');
                redirect('admin/empresas');
            }else{
                $data['id'] = $this->input->post('id');
                $this->load->view('admin/header');
                $this->load->view('admin/empresas/editar', $data);
                $this->load->view('admin/footer');
            }
        }
    }
    
    public function validar_alteracao(){
        $this->validacao->set_rules('razao_social', 'Razão Social', 'required');
        $this->validacao->set_rules('nome_fantasia', 'Nome Fantasia', 'required');
        $this->validacao->set_rules('cnpj', 'CNPJ', 'required');
        $this->validacao->set_rules('registro_ans', 'Registro A.N.S.', 'required');
        $this->validacao->set_rules('categoria', 'Categoria', 'required');
        $this->validacao->set_rules('nome_representante', 'Nome do representante', 'required');
        $this->validacao->set_rules('email_representante', 'E-mail do representante', 'required');
        $this->validacao->set_rules('usuario', 'Usuário', 'required');
        $this->validacao->set_rules('status', 'Status', 'required');
        $this->validacao->set_rules('informativo', 'Informativo', 'required');
        return $this->validacao->run();
    }
	
	public function excluir($id){
        restrictArea();
        if($id){
            $this->info->delete($id);
            $this->session->set_flashdata('msg', 'Informativo excluído com sucesso.');
            redirect('admin/enpresas');
        }
    }

}
/* End of file anunciante.php */
/* Location: ./application/controllers/admin/informativos.php */