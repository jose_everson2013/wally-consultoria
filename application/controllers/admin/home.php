<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation', '', 'validacao');
    }
	
    public function index(){
        if(isLogged()){
            $this->load->view('admin/header');
            $this->load->view('admin/home');
            $this->load->view('admin/footer');
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/logar');
            $this->load->view('admin/footer');
        }			
    }

    public function login(){
        if(isLogged()){
            $this->session->set_flashdata('msg', 'Você já está logado.');
            redirect('admin/home');
        }elseif($this->validar_login()){
            $usuario = $this->input->post('usuario');
            $senha = $this->input->post('senha');
            $this->load->model('admin_model', 'admin');
            $result = $this->admin->login($usuario, $senha);
            if(empty($result)){
                $this->load->model('empresas_model', 'empresa');
                $result = $this->empresa->login($usuario, $senha);
                $data = array('id' => $result[0]->id , 'usuario' => $result[0]->usuario, 'logado' => true, 'tipo_usuario' => 'empresa');
                if(empty($result)){
                    $this->session->set_flashdata('msg', 'Login e/ou senha incorretos.');
                    redirect('admin/home');	
                }
            }else {
                $data = array('id' => $result[0]->id , 'usuario' => $result[0]->usuario, 'logado' => true, 'tipo_usuario' => 'admin');
            }
            $this->session->set_userdata($data);
            redirect('admin/home');
        }else{
            redirect('admin/home');
        }
    }
	
    public function validar_login(){
        $this->validacao->set_rules('usuario', 'Usuário', 'required');
        $this->validacao->set_rules('senha', 'Senha', 'required');
        return $this->validacao->run();
    }
	
    public function logout(){
        if(isLogged()){
            $this->session->sess_destroy();
        }
        redirect('admin/home');
    }
    
    public function recuperar_senha() {
        if(isLogged())
            redirect('admin/home');
        if($this->input->post() && $this->validar_recuperacao_senha()){
            $data = $this->input->post();
            $this->load->model('empresas_model', 'empresa');
            $empresa = $this->empresa->getCompanyByUsernameAndEmail($data);
            if(!empty($empresa)){
                $empresa = (array) $empresa[0];
                $empresa['redefinicao_senha'] = 's';
                $this->empresa->update($empresa);
                $hash = $empresa['usuario'] . $empresa['email_representante'] . $empresa['senha'] . $empresa['redefinicao_senha'];
                $data['hash'] = sha1($hash);
                if($this->enviar_hash($data)){
                    $this->session->set_flashdata('msg', 'Nós enviamos um link para seu email. Siga-o e redefina sua senha!');
                    redirect('admin/home');
                }else{
                    $this->session->set_flashdata('msg', 'Não foi possível recuperar sua senha. Por favor, contacte o administrador do sistema.');
                    redirect('admin/home');
                }
            }else{
                $this->session->set_flashdata('msg', 'Nenhum registro encontrado!');
                redirect('admin/home/recuperar_senha');
            }
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/recuperar_senha');
            $this->load->view('admin/footer');
        }
    }
    
    protected function validar_recuperacao_senha() {
        $this->validacao->set_rules('usuario', 'Usuário', 'required');
        $this->validacao->set_rules('email', 'E-mail', 'required');
        return $this->validacao->run();
    }
    
    protected function enviar_hash($data) {
        $this->load->library('phpmailer/PHPMailer', '', 'mailer');
        $emailSend = 'gleidson@kmcnigro.com.br';
        $this->mailer->CharSet = 'utf-8';
        $this->mailer->IsSMTP();
        //$this->mailer->SMTPDebug  = true;
        $this->mailer->Host       = "smtp.kmcnigro.com.br";
        $this->mailer->Port       = "587";
        $this->mailer->SMTPSecure = "";
        $this->mailer->SMTPAuth   = true;
        $this->mailer->Username   = 'gleidson@kmcnigro.com.br';
        $this->mailer->Password   = "Gl3idonBr1";
        $this->mailer->AddReplyTo('noreply@kmcnigro.com.br');
        $this->mailer->From       = $emailSend;
        $this->mailer->FromName   = "Sistema Wally Consultoria";
        $this->mailer->Subject  = 'Sistema Wally Consultoria - Recuperação de senha';
        $this->mailer->WordWrap = 80;
        $this->mailer->AddAddress($data['email']);
        $msg = $this->load->view('admin/envio_hash', $data, TRUE);
        $this->mailer->MsgHTML($msg, dirname(__FILE__), true);
        if($this->mailer->Send()){
            return true;
        }else{
            return false;
        }
    }
    
    public function validar_hash($hash){
        $this->load->model('empresas_model', 'empresa');
        $empresa = $this->empresa->validateHash($hash);
        if(!empty($empresa)){
            $empresa = (array) $empresa[0];
            $empresa['redefinicao_senha'] = 'n';
            $this->empresa->update($empresa);
            $data = array('id' => $empresa['id'] , 'usuario' => $empresa['usuario'], 'logado' => true, 'tipo_usuario' => 'empresa');
            $this->session->set_userdata($data);
            redirect('admin/home/redefinir_senha');
        }else{
            $this->session->set_flashdata('msg', 'Hash inválido. Tente novamente ou contacte o administrador do sistema!');
            redirect('admin/home');
        }
    }
    
    public function redefinir_senha() {
        if($this->input->post() && $this->validar_redefinicao_senha()){
            $data['senha'] = $this->input->post('nova_senha');
            $data['senha'] = sha1($data['senha']);
            $data['id'] = $this->session->userdata('id');
            $this->load->model('empresas_model', 'empresa');
            if($this->empresa->update($data)){
                $this->session->set_flashdata('msg', 'Senha alterada com sucesso!');
                $this->logout();
            }else{
                $this->session->set_flashdata('msg', 'Ocorreu um erro. Senha não alterada. Por favor entrar em contato com o administrador do sistema.');
                redirect('admin/home');
            }
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/redefinir_senha');
            $this->load->view('admin/footer');
        }
    }
    
    protected function validar_redefinicao_senha() {
        $this->validacao->set_rules('nova_senha', 'Nova Senha', 'required|matches[confirmar_nova_senha]');
        $this->validacao->set_rules('confirmar_nova_senha', 'Confirmar Senha', '');
        return $this->validacao->run();
    }
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */