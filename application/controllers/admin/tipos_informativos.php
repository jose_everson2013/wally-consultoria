<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tipos_informativos extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation', '', 'validacao');
        $this->load->model('tipos_informativos_model', 'tipoInformativo');
        restrictArea();
    }
    
    public function cadastrar() {
        if($this->input->post() && $this->validar_cadastro()){
            $data = $this->input->post();
            $id = $this->tipoInformativo->register($data);
            if($id){
                $this->session->set_flashdata('msg', 'Cadastro efetuado com sucesso!');
                redirect('admin/tipos_informativos');
            }else{
                $this->session->set_flashdata('msg', 'Ocorreu um erro. Tente novamente!');
                redirect('admin/tipos_informativos');
            }
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/tipos_informativos/cadastrar');
            $this->load->view('admin/footer');
        }
    }
    
    public function validar_cadastro() {
        $this->validacao->set_rules('nome', 'Nome', 'required');
        $this->validacao->set_rules('descricao', 'Descrição', '');
        return $this->validacao->run();
    }
    
    public function index() {
        $data['tiposInformativos'] = $this->tipoInformativo->getTypesInformation();
        $this->load->view('admin/header', $data);
        $this->load->view('admin/tipos_informativos/listar');
        $this->load->view('admin/footer');
    }
    
    public function editar($id = null) {
        if($this->input->post()){
            if($this->validar_alteracao_cadastro()){
                $data = $this->input->post();
                if($this->tipoInformativo->update($data)){
                    $this->session->set_flashdata('msg', 'Cadastro alterado com sucesso!');
                    redirect('admin/tipos_informativos');
                }else{
                    $this->session->set_flashdata('msg', 'Ocorreu um erro. Tente novamente!');
                    redirect('admin/tipos_informativos');
                }
            }else{
                $this->load->view('admin/header');
                $this->load->view('admin/tipos_informativos/editar');
                $this->load->view('admin/footer');
            }
        }else{
            if(!empty($id)){
                $data['tipoInformativo'] = $this->tipoInformativo->getTypesInformationById($id);
                if(!empty($data['tipoInformativo'])){
                    $this->load->view('admin/header', $data);
                    $this->load->view('admin/tipos_informativos/editar');
                    $this->load->view('admin/footer');
                }else{
                    $this->session->set_flashdata('msg', 'Ocorreu um erro. Tente novamente!');
                    redirect('admin/tipos_informativos');
                }
            }else{
                $this->session->set_flashdata('msg', 'Ocorreu um erro. Tente novamente!');
                redirect('admin/tipos_informativos');
            }
        }
    }
    
    public function validar_alteracao_cadastro() {
        $this->validacao->set_rules('id', 'Id', 'required');
        $this->validacao->set_rules('nome', 'Nome', 'required');
        $this->validacao->set_rules('descricao', 'Descrição', '');
        return $this->validacao->run();
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/home.php */