<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation', '', 'validacao');
        $this->load->model('admin_model', 'admin');
    }

    public function cadastrar(){
        restrictArea();
        if($this->validar_cadastro()){
            $data = (object) $this->input->post();
            $id = $this->admin->register($data);
            $this->session->set_flashdata('msg', 'Cadastro efetuado com sucesso.');
            redirect('admin/home');
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/usuarios/cadastrar');
            $this->load->view('admin/footer');
        }
    }

    public function validar_cadastro(){
        $this->validacao->set_rules('nome', 'Nome', 'required');
        $this->validacao->set_rules('usuario', 'Nome de usuário', 'required|is_unique[admins.usuario]');
        $this->validacao->set_rules('senha', 'Senha', 'required');
        return $this->validacao->run();
    }

    public function listar(){
        restrictArea();
        $data['admins'] = $this->admin->getAdmins();
        $this->load->view('admin/header');
        $this->load->view('admin/usuarios/listar', $data);
        $this->load->view('admin/footer');
    }
    
    public function editar($id){
        restrictArea();
        if(!$this->input->post()){
            $data['admin'] = $this->admin->getAdmin($id);
            $this->load->view('admin/header');
            $this->load->view('admin/usuarios/editar', $data);
            $this->load->view('admin/footer');
        }else{
            if($this->validar_alteracao()){
                $data = (object) $this->input->post();
                $this->admin->saveChanges($data);
                $this->session->set_flashdata('msg', 'Cadastro atualizado com sucesso.');
                redirect('admin/usuarios/listar');
            }else{
                $data['admin'] = $this->admin->getAdmin($id);
                $this->load->view('admin/header');
                $this->load->view('admin/usuarios/editar', $data);
                $this->load->view('admin/footer');
            }
        }
    }
    
    public function validar_alteracao(){
        $this->validacao->set_rules('nome', 'Nome', 'required');        
        $usuario = $this->admin->getUserNameById($this->input->post('id'));
        if(!($usuario[0]->usuario == $this->input->post('usuario'))){
            $this->validacao->set_rules('usuario', 'Nome de usuário', 'required|is_unique[admins.usuario]');
        }
        return $this->validacao->run();
    }
    
    public function alterar_senha($id){
        restrictArea();
        if($this->input->post()){
            if($this->validar_alteracao_senha()){
                $this->admin->alterPassword($id, $this->input->post('senha'));
                $this->session->set_flashdata('msg', 'Senha alterada com sucesso.');
                redirect('admin/usuarios/listar');
            }else{
                $data['admin'] = $this->admin->getAdmin($id);
                $this->load->view('admin/header');
                $this->load->view('admin/usuarios/editar', $data);
                $this->load->view('admin/footer');
            }
        }else{
            redirect('admin/usuarios/editar/' . $id);
        }
    }
    
    public function validar_alteracao_senha(){
        $this->validacao->set_rules('senha', 'Senha', 'required|matches[confirmar_senha]');
        $this->validacao->set_rules('confirmar_senha', 'Confirmar senha', 'required');
        return $this->validacao->run();
    }
    
    public function excluir($id){
        restrictArea();
        if($id){
            $this->admin->delete($id);
            $this->session->set_flashdata('msg', 'Admin excluído com sucesso.');
            redirect('admin/usuarios/listar');
        }
    }
}

/* End of file admin.php */
/* Location: ./application/controllers/admin/usuarios.php */