<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Funcionarios extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('funcionarios_model', 'func');
        $this->load->model('tipos_informativos_model', 'tipoInfo');
        $this->load->library('form_validation', '', 'validacao');
    }

    public function cadastrar(){
		restrictArea();
		$data['tiposInfos'] = $this->tipoInfo->getTypesInformationByCompanyId($this->session->userdata('id'));
		if($this->validar_cadastro()){
            $data = $this->input->post('func');
            $id = $this->func->register($data);
			$informativo = $this->input->post('informativo');
			$this->load->model('func_tipo_informativo_model', 'funcTipo');
			foreach ($informativo as $value) {
				$data = array (
					'funcionario_id' => $id,
					'tipo_informativo_id' => $value
				);
				$this->funcTipo->register($data);
			}
            $this->session->set_flashdata('msg', 'Cadastro efetuado com sucesso.');
            redirect('admin/home');
        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/funcionarios/cadastrar', $data);
            $this->load->view('admin/footer');
        }
    }

    public function validar_cadastro(){
        $this->validacao->set_rules('func[nome]', 'Nome', 'required');
		$this->validacao->set_rules('func[email]', 'Email', 'required|valid_email|is_unique[funcionarios.email]');
		$this->validacao->set_rules('informativo', 'Tipo de Informativo', 'required');
        return $this->validacao->run();
    }
	
	public function listar(){
        restrictArea();
		$id = $this->session->userdata('id');
        $data['funcionarios'] = $this->func->select_funcionarios($id);
        $this->load->view('admin/header');
        $this->load->view('admin/funcionarios/listar', $data);
        $this->load->view('admin/footer');
    }
	
	public function editar($id){
        restrictArea();
        $data['tiposInfos'] = $this->tipoInfo->getTypesInformationByCompanyId($this->session->userdata('id'));
		$data['empresa_id'] = $this->session->userdata('id');
        $data['funcionario_id'] = $id;
		$this->load->model('func_tipo_informativo_model', 'funcTipo');
		$data['func_tipo_informativo'] = $this->funcTipo->getTypesByEmployeeId($id);
		foreach ($data['tiposInfos'] as $indice => $tipoInfo) {
			$data['tiposInfos'][$indice]->checked = false;
			foreach ($data['func_tipo_informativo'] as $funcTipoInfo) {
				if ($data['tiposInfos'][$indice]->id == $funcTipoInfo->tipo_informativo_id) {
					$data['tiposInfos'][$indice]->checked = true;
				}
			}
		}
        if(!$this->input->post()){
			$data['funcionario'] = $this->func->getEmployeeById($data);
			if (empty($data['funcionario'])) {
				$this->session->set_flashdata('msg', 'Funcionário não encontrado.');
                redirect('admin/funcionarios/listar');
			}
			$this->load->view('admin/header');
            $this->load->view('admin/funcionarios/editar', $data);
            $this->load->view('admin/footer');
        }else{
            if($this->validar_alteracao()){
                $data = $this->input->post('func');
				$this->funcTipo->deleteTypesByEmployeeId($id);
                $this->func->update($data);
				$informativo = $this->input->post('informativo');
				$id = $data['id'];
				foreach ($informativo as $value) {
					$data = array (
						'funcionario_id' => $id,
						'tipo_informativo_id' => $value,
					);
					$this->funcTipo->register($data);
				}
                $this->session->set_flashdata('msg', 'Funcionário atualizada com sucesso.');
                redirect('admin/funcionarios/listar');
            }else{
                $data['funcionario'] = $this->func->getEmployeeById($data);
				if (empty($data['funcionario'])) {
					$this->session->set_flashdata('msg', 'Funcionário não encontrado.');
                	redirect('admin/funcionarios/listar');
				}
                $this->load->view('admin/header');
                $this->load->view('admin/funcionarios/editar', $data);
                $this->load->view('admin/footer');
            }
        }
    }
    
    public function validar_alteracao(){
        $this->validacao->set_rules('func[id]', 'Id', '');
        $this->validacao->set_rules('func[nome]', 'Nome', 'required');
        $this->validacao->set_rules('func[email]', 'Email', 'required|valid_email');
        $this->validacao->set_rules('informativo', 'Tipo de Informativo', 'required');
        return $this->validacao->run();
    }
	
	public function excluir($id){
        restrictArea();
        if($id){
            $this->func->delete($id);
            $this->session->set_flashdata('msg', 'Funcionarios excluído com sucesso.');
            redirect('admin/funcionarios/listar');
        }
    }

}
/* End of file anunciante.php */
/* Location: ./application/controllers/admin/informativos.php */