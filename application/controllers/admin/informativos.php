<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Informativos extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('informativos_model', 'info');
        $this->load->model('tipos_informativos_model', 'tipoInfo');
        $this->load->model('relatorios_envios_model', 'relatorioEnvio');
        $this->load->model('relatorios_destinatarios_model', 'relatorioDest');
        $this->load->model('grupos_empresas_model', 'grupoEmpresa');
        $this->load->model('funcionarios_model', 'func');
        $this->load->library('form_validation', '', 'validacao');
    }

    public function cadastrar(){
        restrictArea();
        if($this->validar_cadastro()){
            $data = $this->input->post();
            if(!empty($data['status'])){
                if(!empty($data['grupo_id'])){
                    $empresas = $this->grupoEmpresa->getCompaniesByGroupId($data['grupo_id']);
                    if(!empty($empresas)){
                        foreach ($empresas as $index => $empresa) {
                            $dados = array(
                                'empresa_id' => $empresa->id,
                                'tipo_informativo_id' => $data['tipo_informativo_id'],
                            );
                            $empresas[$index]->funcionarios = $this->func->getEmploeesByTypeIdAndCompanyId($dados);
                        }
                        foreach ($empresas as $index => $empresa) {
                            $emails[] = $empresa->email_representante;
                            foreach ($empresa->funcionarios as $index => $funcionario) {
                                $emails[] = $funcionario->email;
                            }
                        }
                    }
                }else{
                    $type = $this->input->post('tipo_informativo_id');
                    $this->load->model('func_tipo_informativo_model', 'funcTipo');
                    $funcionarios = $this->funcTipo->getEmployeesByType($type);
                    foreach ($funcionarios as $index => $funcionario) {
                        $emails[] = $funcionario->email_representante;
                        $emails[] = $funcionario->email;
                    }
                }
                if ($this->enviar($emails) == TRUE) {
                    $data['grupo_empresa_id'] = (!empty($data['grupo_id'])) ? $data['grupo_id'] : null;
                    unset($data['grupo_id']);
                    $informativoId = $this->info->register($data);
                    if($informativoId){
                        $relatorio['informativo_id'] = $informativoId;
                        $relatorioId = $this->relatorioEnvio->register($relatorio);
                        $destinatario['relatorio_envio_id'] = $relatorioId;
                        foreach ($this->emails as $index => $email){
                            $destinatario['email_destinatario'] = $email;
                            $this->relatorioDest->register($destinatario);
                        }
                        $this->session->set_flashdata('msg', 'Informativo cadastrado e enviado com sucesso.');
                        redirect('admin/informativos');
                    }else{
                        $this->session->set_flashdata('msg', 'Informativo enviado com sucesso, mas ocorreu um erro ao cadastrá-lo na base de dados.');
                        redirect('admin/informativos');
                    }
                }else{
                    unset($data['grupo_id']);
                    if($this->info->register($data)){
                        $this->session->set_flashdata('msg', 'Informativo cadastrado com sucesso, mas não foi possível ser enviado.');
                        redirect('admin/informativos');
                    }else{
                        $this->session->set_flashdata('msg', 'Ocorreu um erro ao enviar e cadastrar o informativo.');
                        redirect('admin/informativos');
                    }
                }
            }else{
                unset($data['grupo_id']);
                if($data['tipo_informativo_id'] == 'site'){
                    unset($data['tipo_informativo_id']);unset($data['grupo_id']);
                    $data['status'] = 's';
                }
                if($this->info->register($data)){
                    $this->session->set_flashdata('msg', 'Informativo cadastrado com sucesso.');
                    redirect('admin/informativos');
                }else{
                    $this->session->set_flashdata('msg', 'Erro ao cadastrar informativo.');
                    redirect('admin/informativos');
                }
            }
        }else{
            $data['tiposInfos'] = $this->tipoInfo->getTypesInformation();
            $data['gruposEmpresas'] = $this->grupoEmpresa->getCompaniesGroups();
            $this->load->view('admin/header', $data);
            $this->load->view('admin/informativos/cadastrar');
            $this->load->view('admin/footer');
        }
    }
	
    public function enviar($emails = null){
        $this->load->library('phpmailer/PHPMailer', '', 'mailer');
        foreach ($emails as $email) {
            $this->mailer->AddBCC($email);
            $this->emails[] = $email;
        }
        $emailSend = 'informativo@wallyconsultoria.com.br';
        $this->mailer->CharSet = 'utf-8';
        $this->mailer->IsSMTP();
        //$this->mailer->SMTPDebug  = true;
        $this->mailer->Host       = "smtp.wallyconsultoria.com.br";
        $this->mailer->Port       = "587";
        $this->mailer->SMTPAuth   = true;
        $this->mailer->SMTPSecure = 'tls';
        $this->mailer->Username   = $emailSend;
        $this->mailer->Password   = "info@0311";
        $this->mailer->AddReplyTo($emailSend);
        $this->mailer->From       = $emailSend;
        $this->mailer->FromName   = "Sistema Wally Consultoria";
        $this->mailer->Subject  = $this->input->post('titulo', TRUE);
        $this->mailer->WordWrap = 80;
        $dados['titulo'] = $this->input->post('titulo', TRUE);
        $dados['conteudo'] = $this->input->post('conteudo', TRUE);
        $msg = $this->load->view('email', $dados, TRUE);
        $this->mailer->MsgHTML($msg, dirname(__FILE__), true);
        if($this->mailer->Send()){
            return true;
        }else{
            return false;
        }
    }

    public function validar_cadastro(){
        $this->validacao->set_rules('titulo', 'Título', 'required');
        $this->validacao->set_rules('conteudo', 'Conteúdo', 'required');
        $this->validacao->set_rules('tipo_informativo_id', 'Tipo de informativo', 'required');
        return $this->validacao->run();
    }
	
	public function index(){
        restrictArea();
        $data['infos'] = $this->info->select_informativos();
        $this->load->view('admin/header');
        $this->load->view('admin/informativos/listar', $data);
        $this->load->view('admin/footer');
    }
	
    public function editar($id){
        restrictArea();
        $data['tiposInfos'] = $this->tipoInfo->getTypesInformation();
        if(!$this->input->post()){
            $data['gruposEmpresas'] = $this->grupoEmpresa->getCompaniesGroups();
            $data['info'] = $this->info->getInfomationById($id);
            $this->load->view('admin/header');
            $this->load->view('admin/informativos/editar', $data);
            $this->load->view('admin/footer');
        }else{
            if($this->validar_alteracao()){
                $data = $this->input->post();
                if(!empty($data['status'])){
                    if(!empty($data['grupo_id'])){
                        $empresas = $this->grupoEmpresa->getCompaniesByGroupId($data['grupo_id']);
                        if(!empty($empresas)){
                            foreach ($empresas as $index => $empresa) {
                                $dados = array(
                                    'empresa_id' => $empresa->id,
                                    'tipo_informativo_id' => $data['tipo_informativo_id'],
                                );
                                $empresas[$index]->funcionarios = $this->func->getEmploeesByTypeIdAndCompanyId($dados);
                            }
                            foreach ($empresas as $index => $empresa) {
                                $emails[] = $empresa->email_representante;
                                foreach ($empresa->funcionarios as $index => $funcionario) {
                                    $emails[] = $funcionario->email;
                                }
                            }
                        }
                    }else{
                        $type = $this->input->post('tipo_informativo_id');
                        $this->load->model('func_tipo_informativo_model', 'funcTipo');
                        $funcionarios = $this->funcTipo->getEmployeesByType($type);
                        foreach ($funcionarios as $index => $funcionario) {
                            $emails[] = $funcionario->email_representante;
                            $emails[] = $funcionario->email;
                        }
                    }
                    if ($this->enviar($emails) == TRUE) {
                        $data['grupo_empresa_id'] = $data['grupo_id'];
                        unset($data['grupo_id']);
                        if($this->info->update($data)){
                            $informativoId = $data['id'];
                            $relatorio['informativo_id'] = $informativoId;
                            $relatorioId = $this->relatorioEnvio->register($relatorio);
                            $destinatario['relatorio_envio_id'] = $relatorioId;
                            foreach ($this->emails as $index => $email){
                                $destinatario['email_destinatario'] = $email;
                                $this->relatorioDest->register($destinatario);
                            }
                            $this->session->set_flashdata('msg', 'Informativo salvo e enviado com sucesso.');
                            redirect('admin/informativos');
                        }else{
                            $this->session->set_flashdata('msg', 'Informativo enviado com sucesso, mas ocorreu um erro ao salvá-lo na base de dados.');
                            redirect('admin/informativos');
                        }
                    }else{
                        $data['status'] = 'r';
                        $data['grupo_empresa_id'] = $data['grupo_id'];
                        unset($data['grupo_id']);
                        if($this->info->update($data)){
                            $this->session->set_flashdata('msg', 'Informativo cadastrado com sucesso, mas não foi possível ser enviado.');
                            redirect('admin/informativos');
                        }else{
                            $this->session->set_flashdata('msg', 'Ocorreu um erro ao enviar e cadastrar o informativo.');
                            redirect('admin/informativos');
                        }
                    }
                }else{
                    unset($data['grupo_id']);
                    if($data['tipo_informativo_id'] == 'site'){
                        unset($data['tipo_informativo_id']);
                        $data['status'] = 's';
                    }
                    if($this->info->update($data)){
                        $this->session->set_flashdata('msg', 'Informativo atualizado com sucesso.');
                        redirect('admin/informativos');
                    }else{
                        $this->session->set_flashdata('msg', 'Erro ao atualizar o informativo.');
                        redirect('admin/informativos');
                    }
                }
            }else{
                $data['gruposEmpresas'] = $this->grupoEmpresa->getCompaniesGroups();
                $data['info'] = $this->info->getInfomationById($id);
                $this->load->view('admin/header');
                $this->load->view('admin/informativos/editar', $data);
                $this->load->view('admin/footer');
            }
        }
    }
    
    public function validar_alteracao(){
        $this->validacao->set_rules('titulo', 'Título', 'required');
        $this->validacao->set_rules('conteudo', 'Conteúdo', 'required');
        $this->validacao->set_rules('tipo_informativo_id', 'Tipo de informativo', 'required');
        return $this->validacao->run();
    }
	
	public function excluir($id){
        restrictArea();
        if($id){
            $this->info->delete($id);
            $this->session->set_flashdata('msg', 'Informativo excluído com sucesso.');
            redirect('admin/informativos');
        }
    }

    public function visualizar($id = null){
        restrictArea();
        if(!empty($id)){
            $data['informativo'] = $this->info->getInfomationById($id);
            if(!empty($data['informativo'])){
                $dados['titulo'] = $data['informativo'][0]->titulo;
                $dados['conteudo'] = $data['informativo'][0]->conteudo;
                $dados['relatorioEnvio'] = $this->relatorioEnvio->getReportByInformationId($data['informativo'][0]->id);
                if(!empty($dados['relatorioEnvio'])){
                    $dados['destinatarios'] = $this->relatorioDest->getRecipientsByReportId($dados['relatorioEnvio'][0]->id);
                }
                $this->load->view('admin/header');
                $this->load->view('admin/informativos/visualizar', $dados);
                $this->load->view('admin/footer');
            }else{
                $this->session->set_flashdata('msg', 'Nenhum registro encontrado!');
                redirect('admin/informativos');
            }
        }else{
            redirect('admin/informativos');
        }
    }

}
/* End of file anunciante.php */
/* Location: ./application/controllers/admin/informativos.php */