<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
            parent::__construct();
            $this->menu = $this->load->view('menu', '', true);
            $this->load->library('form_validation', '', 'validacao');
            $this->validacao->set_error_delimiters('<span class="error">', '</span>');
        }
	
	public function index(){
            $this->load->model('informativos_model', 'info');
            $data = array (
                'menu' => $this->menu,
                'exibe' => $this->info->getInformationForSite(),
            );
            $informativos1 = new SimpleXMLElement('http://www.ans.gov.br/rss/feed.php', null, true);
            $informativos2 = new SimpleXMLElement('http://www.ans.gov.br/index.php/a-ans/sala-de-noticias-ans?format=feed&type=rss', null, true);
            foreach ($informativos1->channel->item as $item) {
                $data['informativos'][] = $item;
            }
            foreach ($informativos2->channel->item as $item) {
                $data['informativos'][] = $item;
            }
            shuffle($data['informativos']);
            $this->load->view('header', $data);
            $this->load->view('home');
            $this->load->view('footer');
	}
	
	public function quem_somos()
	{
		$data['menu'] = $this->menu;
		$this->load->view('header');
		$this->load->view('quem_somos', $data);
		$this->load->view('footer');
	}
	
	public function servicos()
	{
		$data['menu'] = $this->menu;
		$this->load->view('header');
		$this->load->view('servicos', $data);
		$this->load->view('footer');
	}
	
	public function informativos(){
            $this->load->model('informativos_model', 'info');
            $dados = array (
                'menu' => $this->menu,
                'exibe' => $this->info->getInformationForSite(),
            );
            $this->load->view('header');
            $this->load->view('informativos', $dados);
            $this->load->view('footer');
	}
	
	public function contato()
	{
		$data['menu'] = $this->menu;
		$this->load->view('header');
		$this->load->view('contato', $data);
		$this->load->view('footer');
	}
	
	public function logar()
	{
            $this->load->view('admin/header');
			$this->load->view('admin/home');
			$this->load->view('admin/footer');		
	}
	
	public function login(){
        if(isLogged()){
            $this->session->set_flashdata('msg', 'Você já está logado.');
            redirect('admin/home');
        }elseif($this->validar_login()){
            $usuario = $this->input->post('usuario');
            $senha = $this->input->post('senha');
                $this->load->model('admin_model', 'admin');
                $result = $this->admin->login($usuario, $senha);
                if(empty($result)){
					$this->load->model('empresas_model', 'empresa');
                	$result = $this->empresa->login($usuario, $senha);
					$data = array('id' => $result[0]->id , 'usuario' => $result[0]->usuario, 'logado' => true, 'tipo_usuario' => 'empresa');
					if(empty($result)){
						$this->session->set_flashdata('msg', 'Login e/ou senha incorretos.');
						redirect('admin/home');	
					}
                }
				else {
                	$data = array('id' => $result[0]->id , 'usuario' => $result[0]->usuario, 'logado' => true, 'tipo_usuario' => 'admin');
				}
				$this->load->view('admin/home');
                $this->session->set_userdata($data);
                redirect('admin/home');
		}else{
            $this->logar();
        }
	}
	
	public function validar_login(){
        $this->validacao->set_rules('usuario', 'Usuário', 'required');
        $this->validacao->set_rules('senha', 'Senha', 'required');
        return $this->validacao->run();
    }
	
	function enviar()
	{
		if($this->validar_envio()){
		
		$config['protocol'] = 'mail';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html'; 
		$config['crlf'] = '\r\n';
		$config['newline'] = '\r\n';	
		$de = $this->input->post('email', TRUE);        //CAPTURA O VALOR DA CAIXA DE TEXTO 'E-mail Remetente'
		$para = 'teste@kmcmarketing.com.br';    //CAPTURA O VALOR DA CAIXA DE TEXTO 'E-mail de Destino'
		$nome = $this->input->post('nome', TRUE);      //CAPTURA O VALOR DA CAIXA DE TEXTO 'Nome'
		$empresa = $this->input->post('empresa', TRUE);      //CAPTURA O VALOR DA CAIXA DE TEXTO 'Nome'
		$email = $this->input->post('email', TRUE);      //CAPTURA O VALOR DA CAIXA DE TEXTO 'Nome'
		$telefone = $this->input->post('telephone', TRUE);      //CAPTURA O VALOR DA CAIXA DE TEXTO 'Nome'
		$msg = $this->input->post('message', TRUE);      //CAPTURA O VALOR DA CAIXA DE TEXTO 'Nome'
		$this->load->library('email');                   //CARREGA A CLASSE EMAIL DENTRO DA LIBRARY DO FRAMEWORK
		$this->email->from($de, $nome);                //ESPECIFICA O FROM(REMETENTE) DA MENSAGEM DENTRO DA CLASSE
		$this->email->to($para);                         //ESPECIFICA O DESTINATÁRIO DA MENSAGEM DENTRO DA CLASSE  
		$this->email->subject('Contato Wally Consultoria');         //ESPECIFICA O ASSUNTO DA MENSAGEM DENTRO DA CLASSE
		$this->email->message("<html><head></head><body><table><tr><td>Nome:</td><td>$nome</td></tr><tr><td>Email:</td><td>$email</td></tr><tr><td>Empresa:</td><td>$empresa</td></tr><tr><td>Telefone:</td><td>$telefone</td></tr><tr><td>Mensagem:</td><td>$msg</td></tr></table></body></html>");	                 //ESPECIFICA O TEXTO DA MENSAGEM DENTRO DA CLASSE
		$this->email->send();                            //AÇÃO QUE ENVIA O E-MAIL COM OS PARÂMETROS DEFINIDOS ANTERIORMENTE
		//echo $this->email->print_debugger();             //COMANDO QUE MOSTRA COMO ACONTECEU O ENVIO DA MENSAGEM NO SERVIDOR
		$this->load->view('enviou');                     //CARREGA A VIEW 'enviou'
		}
		else {
			$data['menu'] = $this->menu;
			$this->load->view('header');
			$this->load->view('contato', $data);
			$this->load->view('footer');
		}
	}
	
	public function validar_envio(){
            $this->validacao->set_rules('nome', 'Nome', 'required');
            $this->validacao->set_rules('email', 'Email', 'required|valid_email');
            $this->validacao->set_rules('message', 'Mensagem', 'required');
            $this->validacao->set_message('required', '%s é requerido');
            return $this->validacao->run();
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */