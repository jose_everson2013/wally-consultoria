<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!function_exists('restrictArea')){
    function restrictArea(){
        $CI = & get_instance();
        $id_sessao = $CI->session->userdata('session_id');
        $logado = $CI->session->userdata('logado');
        if(!$id_sessao || !$logado){
            redirect('admin/home');
        }
    }
}

if(!function_exists('url_css')){
    function url_css($file){
        return site_url('assets/css/' . $file);
    }
}
if(!function_exists('url_img')){
    function url_img($file){
        return site_url('assets/img/' . $file);
    }
}
if(!function_exists('url_plugin')){
    function url_plugin($file){
        return site_url('assets/plugin/' . $file);
    }
}
if(!function_exists('url_js')){
    function url_js($file){
        return site_url('assets/js/' . $file);
    }
}
if(!function_exists('url_up')){
    function url_up($file){
        return site_url('assets/uploads/' . $file);
    }
}

if(!function_exists('isActivePage')){
    function isActivePage($page){
        $CI = & get_instance();
        if($page == 'home' && $CI->uri->segment(1) == false){
            return 'class="active"';
        }
        return ($CI->uri->segment(1) == $page) ? 'class="active"' : '';
    }
}

if(!function_exists('isLogged')){
    function isLogged(){
        $CI = & get_instance();
        $logado = $CI->session->userdata('logado');
        if(empty($logado)){
            return false;
        }else{
            return true;
        }
    }
}

if(!function_exists('isAdmin')){
    function isAdmin(){
        $CI = & get_instance();
        $admin = $CI->session->userdata('tipo_usuario');
        if($admin == 'admin'){
            return true;
        }else{
            return false;
        }
    }
}

if(!function_exists('getUserName')){
    function getUserName(){
        $CI = & get_instance();
        $userName = $CI->session->userdata('usuario');
        if(!$userName){
            return false;
        }else{
            return $userName;
        }
    }
}

if(!function_exists('getId')){
    function getId(){
        $CI = & get_instance();
        $id = $CI->session->userdata('id');
        if(!$id){
            return false;
        }else{
            return $id;
        }
    }
}

if(!function_exists('getFlashdata')){
    function getFlashdata($item = null){
        $CI = & get_instance();
        $msg = $CI->session->flashdata($item);
        if($msg){
            return $msg;
        }else{
            return false;
        }
    }
}
/* End of file default_helper.php */
/* Location: ./application/helpers/general_helper.php */