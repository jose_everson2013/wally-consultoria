-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 26, 2013 at 06:06 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wally`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `senha` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `nome`, `usuario`, `senha`) VALUES
(1, 'teste', 'teste', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(100) NOT NULL,
  `nome_fantasia` varchar(100) NOT NULL,
  `cnpj` varchar(20) NOT NULL,
  `registro_ans` varchar(7) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `nome_representante` varchar(255) NOT NULL,
  `email_representante` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `status` enum('a','d','b') NOT NULL DEFAULT 'a',
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='	' AUTO_INCREMENT=14 ;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`id`, `razao_social`, `nome_fantasia`, `cnpj`, `registro_ans`, `categoria`, `nome_representante`, `email_representante`, `usuario`, `senha`, `status`, `data_cadastro`) VALUES
(4, 'sdsd', 'sdsd', '06.990.590/0001-23', '1546515', 'Saúde', 'Hugo', 'gleidson@kmcnigro.com.br', 'tst', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'a', '2013-06-30 00:39:46'),
(6, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '0011155446688779', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'd', '2013-07-18 20:52:06'),
(7, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 20:47:50'),
(8, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '1436e28c07e79c44c7939b0d8c2e931107ba53cc', 'a', '2013-07-19 20:53:33'),
(9, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:01:00'),
(10, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:03:00'),
(11, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:04:47'),
(12, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:05:05'),
(13, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:06:04');

-- --------------------------------------------------------

--
-- Table structure for table `empresas_participantes_grupos`
--

CREATE TABLE IF NOT EXISTS `empresas_participantes_grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreignkey_empresa_grupo_1_idx` (`empresa_id`),
  KEY `foreignkey_empresa_grupo_2_idx` (`grupo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `empresas_participantes_grupos`
--

INSERT INTO `empresas_participantes_grupos` (`id`, `empresa_id`, `grupo_id`) VALUES
(33, 4, 1),
(35, 6, 1),
(36, 7, 1),
(37, 8, 1),
(38, 9, 1),
(39, 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `empresa_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `empresa_tipo_informativo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `tipo_informativo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreign_key_empresa_tipo_informativo_2_idx` (`tipo_informativo_id`),
  KEY `foreign_key_empresa_tipo_informativo_1_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `empresa_tipo_informativo`
--

INSERT INTO `empresa_tipo_informativo` (`id`, `empresa_id`, `tipo_informativo_id`) VALUES
(26, 7, 3),
(27, 8, 3),
(28, 9, 3),
(29, 10, 3),
(30, 11, 3),
(31, 12, 3),
(32, 13, 3),
(33, 6, 2),
(34, 6, 3),
(35, 4, 1),
(36, 4, 2),
(37, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `data_cadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `funcionario_fk_empresa_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `funcionarios`
--

INSERT INTO `funcionarios` (`id`, `nome`, `email`, `empresa_id`, `data_cadastro`) VALUES
(3, 'sd', 'nosrev09@gmail.com', 4, '2013-06-30 15:33:01'),
(12, 'teste10', 'jose@kmcnigro.com.br', 6, '2013-07-02 21:01:12'),
(13, 'new', 'new@new.com.br', 4, '2013-07-03 17:35:11');

-- --------------------------------------------------------

--
-- Table structure for table `func_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `func_tipo_informativo` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `funcionario_id` int(11) NOT NULL,
  `tipo_informativo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `func_tipo_funcionario_id` (`funcionario_id`),
  KEY `func_tipo_informativo_ibfk_2_idx` (`tipo_informativo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `func_tipo_informativo`
--

INSERT INTO `func_tipo_informativo` (`id`, `funcionario_id`, `tipo_informativo_id`) VALUES
(2, 12, 2),
(3, 12, 1),
(15, 3, 1),
(16, 3, 2),
(17, 3, 3),
(18, 13, 1),
(19, 13, 2),
(20, 13, 3);

-- --------------------------------------------------------

--
-- Table structure for table `grupos_empresas`
--

CREATE TABLE IF NOT EXISTS `grupos_empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='				' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `grupos_empresas`
--

INSERT INTO `grupos_empresas` (`id`, `nome`, `data_criacao`) VALUES
(1, 'Seguradoras', '2013-07-22 22:24:21');

-- --------------------------------------------------------

--
-- Table structure for table `informativos`
--

CREATE TABLE IF NOT EXISTS `informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `conteudo` text NOT NULL,
  `tipo_informativo_id` int(11) DEFAULT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` enum('s','r','e') NOT NULL DEFAULT 'r',
  `grupo_empresa_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_informativos_1` (`tipo_informativo_id`),
  KEY `fk_informativos_2_idx` (`grupo_empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=133 ;

--
-- Dumping data for table `informativos`
--

INSERT INTO `informativos` (`id`, `titulo`, `conteudo`, `tipo_informativo_id`, `data_cadastro`, `status`, `grupo_empresa_id`) VALUES
(2, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171', '<p>A operadora ____(RAZ&Atilde;O SOCIAL DA OPERADORA)____, inscrita no CNPJ/MF sob o n&ordm; _____(CNPJ)____ e registrada na ANS sob o n&ordm; ___(REGISTRO)__, vem solicitar &aacute;  Ag&ecirc;ncia Nacional de Sa&uacute;de Suplementar - ANS, autoriza&ccedil;&atilde;o para aplica&ccedil;&atilde;o de reajuste da contrapresta&ccedil;&atilde;o pecun&aacute;ria aos planos individuais e familiares, no m&aacute;ximo no percentual estabelecido pela ANS, conforme previsto na RN n&ordm; 171/08, no per&iacute;odo compreendido entre os meses de maio/2013 e abril/2014.</p>', 1, '2013-07-25 17:54:19', 'e', 1),
(3, 'sdsd', '<p>sdsd</p>\r\n', NULL, '2013-07-25 17:54:19', 'r', NULL),
(4, 'sdsd', '<p>sdd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(5, 'bnbnbn', '<p>bnnbn</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(6, 'asfdsfdsf', '<p>ghnngng</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(7, 'svsdvdsvs', '<p>vdvdsvsv</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(8, 'svsdvdsvs', '<p>vdvdsvsv</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(9, 'svsdvdsvs', '<p>vdvdsvsv</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(10, 'ererere', '<p>ererer</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(11, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(12, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(13, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(14, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(15, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(16, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(17, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(18, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(19, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(20, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(21, 'dsd', '<p>sd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(22, 'dsd', '<p>sd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(23, 'sdsd', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(24, 'sdsd', '<p>dssd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(25, 'sdsd', '<p>dssd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(26, 'wcwcw', '<p>sxcwc</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(27, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(28, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(29, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(30, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(31, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(32, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(33, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(34, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(35, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(36, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(37, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(38, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(39, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(40, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(41, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(42, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(43, 'dsdsds', '<p>sdsd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(44, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(45, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(46, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(47, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(48, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(49, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(50, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(51, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(52, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(53, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(54, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(55, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(56, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(57, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(58, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(59, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(60, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(61, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(62, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(63, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(64, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(65, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(66, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(67, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(68, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(69, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(70, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(71, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(72, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(73, 'sdsdsd', '<p>ds</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(74, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(75, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(76, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(77, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(78, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(79, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(80, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(81, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(82, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(83, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(84, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(85, 'bdbdb', '<p>bdbd</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(86, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(87, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(88, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(89, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(90, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(91, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(92, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(93, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(94, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(95, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(96, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(97, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(98, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(99, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(100, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(101, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(102, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(103, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(104, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(105, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(106, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(107, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(108, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(109, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(110, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(111, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(112, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(113, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(114, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(115, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(116, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(117, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(118, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(119, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(120, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(121, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(122, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(123, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(124, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(125, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(126, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(127, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(128, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(129, 'dqdq', '<p>wqdqwdqw</p>', NULL, '2013-07-25 17:54:19', 'r', NULL),
(131, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171', '<p>A operadora ____(RAZ&Atilde;O SOCIAL DA OPERADORA)____, inscrita no CNPJ/MF sob o n&ordm; _____(CNPJ)____ e registrada na ANS sob o n&ordm; ___(REGISTRO)__, vem solicitar &aacute; Ag&ecirc;ncia Nacional de Sa&uacute;de Suplementar - ANS, autoriza&ccedil;&atilde;o para aplica&ccedil;&atilde;o de reajuste da contrapresta&ccedil;&atilde;o pecun&aacute;ria aos planos individuais e familiares, no m&aacute;ximo no percentual estabelecido pela ANS, conforme previsto na RN n&ordm; 171/08, no per&iacute;odo compreendido entre os meses de maio/2013 e abril/2014.</p>', 1, '2013-07-25 19:37:12', 'e', 1),
(132, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171', '<p><span>A operadora ____(RAZ&Atilde;O SOCIAL DA OPERADORA)____, inscrita no CNPJ/MF sob o n&ordm; _____(CNPJ)____ e registrada na ANS sob o n&ordm; ___(REGISTRO)__, vem solicitar &aacute; Ag&ecirc;ncia Nacional de Sa&uacute;de Suplementar - ANS, autoriza&ccedil;&atilde;o para aplica&ccedil;&atilde;o de reajuste da contrapresta&ccedil;&atilde;o pecun&aacute;ria aos planos individuais e familiares, no m&aacute;ximo no percentual estabelecido pela ANS, conforme previsto na RN n&ordm; 171/08, no per&iacute;odo compreendido entre os meses de maio/2013 e abril/2014.</span></p>', NULL, '2013-07-25 20:07:33', 's', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `relatorios_destinatarios`
--

CREATE TABLE IF NOT EXISTS `relatorios_destinatarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_destinatario` varchar(100) DEFAULT NULL,
  `relatorio_envio_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_relatorios_destinatarios_1` (`relatorio_envio_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `relatorios_destinatarios`
--

INSERT INTO `relatorios_destinatarios` (`id`, `email_destinatario`, `relatorio_envio_id`) VALUES
(1, 'gleidson@kmcnigro.com.br', 1),
(2, 'nosrev09@gmail.com', 1),
(3, 'new@new.com.br', 1),
(4, 'gleidson@kmcnigro.com.br', 1),
(5, 'jose@kmcnigro.com.br', 1),
(6, 'gleidson@kmcnigro.com.br', 1),
(7, 'gleidson@kmcnigro.com.br', 1),
(8, 'gleidson@kmcnigro.com.br', 1),
(9, 'gleidson@kmcnigro.com.br', 1),
(10, 'gleidson@kmcnigro.com.br', 2),
(11, 'nosrev09@gmail.com', 2),
(12, 'new@new.com.br', 2),
(13, 'gleidson@kmcnigro.com.br', 2),
(14, 'jose@kmcnigro.com.br', 2),
(15, 'gleidson@kmcnigro.com.br', 2),
(16, 'gleidson@kmcnigro.com.br', 2),
(17, 'gleidson@kmcnigro.com.br', 2),
(18, 'gleidson@kmcnigro.com.br', 2);

-- --------------------------------------------------------

--
-- Table structure for table `relatorios_envios`
--

CREATE TABLE IF NOT EXISTS `relatorios_envios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_envio` datetime DEFAULT NULL,
  `informativo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_relatorios_envios_1` (`informativo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `relatorios_envios`
--

INSERT INTO `relatorios_envios` (`id`, `data_envio`, `informativo_id`) VALUES
(1, '2013-07-25 10:36:47', 131),
(2, '2013-07-25 10:51:51', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tipos_informativos`
--

CREATE TABLE IF NOT EXISTS `tipos_informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tipos_informativos`
--

INSERT INTO `tipos_informativos` (`id`, `nome`, `descricao`) VALUES
(1, 'Normas', 'As normas são informativos periódicos e importantes, que serão enviados quinzenalmente.'),
(2, 'Lembretes', 'Os lembretes são informativos periódicos, que serão enviados semanalmente. Tem o objetivo de lembrar os colaboradores de obrigações.'),
(3, 'Normas jurídicas', 'A norma jurídica é a célula do ordenamento jurídico. É um imperativo de conduta, que coage os sujeitos a se comportarem da forma por ela esperada e desejada.');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `empresas_participantes_grupos`
--
ALTER TABLE `empresas_participantes_grupos`
  ADD CONSTRAINT `foreignkey_empresa_grupo_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `foreignkey_empresa_grupo_2` FOREIGN KEY (`grupo_id`) REFERENCES `grupos_empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `empresa_tipo_informativo`
--
ALTER TABLE `empresa_tipo_informativo`
  ADD CONSTRAINT `foreign_key_empresa_tipo_informativo_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `foreign_key_empresa_tipo_informativo_2` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD CONSTRAINT `funcionario_fk_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `func_tipo_informativo`
--
ALTER TABLE `func_tipo_informativo`
  ADD CONSTRAINT `func_tipo_informativo_ibfk_1` FOREIGN KEY (`funcionario_id`) REFERENCES `funcionarios` (`id`),
  ADD CONSTRAINT `func_tipo_informativo_ibfk_2` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `informativos`
--
ALTER TABLE `informativos`
  ADD CONSTRAINT `fk_informativos_1` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_informativos_2` FOREIGN KEY (`grupo_empresa_id`) REFERENCES `grupos_empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `relatorios_destinatarios`
--
ALTER TABLE `relatorios_destinatarios`
  ADD CONSTRAINT `fk_relatorios_destinatarios_1` FOREIGN KEY (`relatorio_envio_id`) REFERENCES `relatorios_envios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `relatorios_envios`
--
ALTER TABLE `relatorios_envios`
  ADD CONSTRAINT `fk_relatorios_envios_1` FOREIGN KEY (`informativo_id`) REFERENCES `informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
