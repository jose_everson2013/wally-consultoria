-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 25, 2013 at 08:40 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wally`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `senha` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `nome`, `usuario`, `senha`) VALUES
(1, 'teste', 'teste', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(100) NOT NULL,
  `nome_fantasia` varchar(100) NOT NULL,
  `cnpj` varchar(20) NOT NULL,
  `registro_ans` varchar(7) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `nome_representante` varchar(255) NOT NULL,
  `email_representante` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `status` enum('a','d','b') NOT NULL DEFAULT 'a',
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='	' AUTO_INCREMENT=14 ;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`id`, `razao_social`, `nome_fantasia`, `cnpj`, `registro_ans`, `categoria`, `nome_representante`, `email_representante`, `usuario`, `senha`, `status`, `data_cadastro`) VALUES
(4, 'sdsd', 'sds', '', '', '', '', '', 'tst', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'a', '2013-06-30 00:39:46'),
(5, 'teste', 'teste', '', '', '', '', '', 'empresa2', '123', 'a', '2013-07-02 21:43:29'),
(6, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '0011155446688779', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'd', '2013-07-18 20:52:06'),
(7, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 20:47:50'),
(8, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '1436e28c07e79c44c7939b0d8c2e931107ba53cc', 'a', '2013-07-19 20:53:33'),
(9, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:01:00'),
(10, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:03:00'),
(11, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:04:47'),
(12, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:05:05'),
(13, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '06.990.590/0001-23', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-19 21:06:04');

-- --------------------------------------------------------

--
-- Table structure for table `empresas_participantes_grupos`
--

CREATE TABLE IF NOT EXISTS `empresas_participantes_grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreignkey_empresa_grupo_1_idx` (`empresa_id`),
  KEY `foreignkey_empresa_grupo_2_idx` (`grupo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `empresas_participantes_grupos`
--

INSERT INTO `empresas_participantes_grupos` (`id`, `empresa_id`, `grupo_id`) VALUES
(29, 4, 1),
(30, 5, 1),
(31, 6, 1),
(32, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `empresa_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `empresa_tipo_informativo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `tipo_informativo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreign_key_empresa_tipo_informativo_2_idx` (`tipo_informativo_id`),
  KEY `foreign_key_empresa_tipo_informativo_1_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `empresa_tipo_informativo`
--

INSERT INTO `empresa_tipo_informativo` (`id`, `empresa_id`, `tipo_informativo_id`) VALUES
(26, 7, 3),
(27, 8, 3),
(28, 9, 3),
(29, 10, 3),
(30, 11, 3),
(31, 12, 3),
(32, 13, 3),
(33, 6, 2),
(34, 6, 3);

-- --------------------------------------------------------

--
-- Table structure for table `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `data_cadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `funcionario_fk_empresa_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `funcionarios`
--

INSERT INTO `funcionarios` (`id`, `nome`, `email`, `empresa_id`, `data_cadastro`) VALUES
(3, 'sd', 'nosrev09@gmail.com', 4, '2013-06-30 15:33:01'),
(12, 'teste10', 'jose@kmcnigro.com.br', 5, '2013-07-02 21:01:12'),
(13, 'new', 'new@new.com.br', 4, '2013-07-03 17:35:11');

-- --------------------------------------------------------

--
-- Table structure for table `func_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `func_tipo_informativo` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `funcionario_id` int(11) NOT NULL,
  `tipo_informativo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `func_tipo_funcionario_id` (`funcionario_id`),
  KEY `func_tipo_informativo_ibfk_2_idx` (`tipo_informativo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `func_tipo_informativo`
--

INSERT INTO `func_tipo_informativo` (`id`, `funcionario_id`, `tipo_informativo_id`) VALUES
(1, 3, 0),
(2, 12, 0),
(3, 12, 0),
(14, 13, 0);

-- --------------------------------------------------------

--
-- Table structure for table `grupos_empresas`
--

CREATE TABLE IF NOT EXISTS `grupos_empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='				' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `grupos_empresas`
--

INSERT INTO `grupos_empresas` (`id`, `nome`, `data_criacao`) VALUES
(1, 'Seguradoras', '2013-07-22 22:24:21');

-- --------------------------------------------------------

--
-- Table structure for table `informativos`
--

CREATE TABLE IF NOT EXISTS `informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `conteudo` text NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('a','b','r') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

--
-- Dumping data for table `informativos`
--

INSERT INTO `informativos` (`id`, `titulo`, `conteudo`, `tipo`, `data_cadastro`, `status`) VALUES
(2, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171', 'A operadora ____(RAZÃO SOCIAL DA OPERADORA)____, inscrita no CNPJ/MF sob o nº _____(CNPJ)____ e registrada na ANS sob o nº ___(REGISTRO)__, vem solicitar á  Agência Nacional de Saúde Suplementar - ANS, autorização para aplicação de reajuste da contraprestação pecunária aos planos individuais e familiares, no máximo no percentual estabelecido pela ANS, conforme previsto na RN nº 171/08, no período compreendido entre os meses de maio/2013 e abril/2014.', 'noticia', '2013-07-03 13:46:25', NULL),
(3, 'sdsd', '<p>sdsd</p>\r\n', 'noticia', '2013-07-03 13:47:15', NULL),
(4, 'sdsd', '<p>sdd</p>', 'norma', '2013-07-03 13:50:53', NULL),
(5, 'bnbnbn', '<p>bnnbn</p>', 'lembrete', '2013-07-03 13:51:35', NULL),
(6, 'asfdsfdsf', '<p>ghnngng</p>', 'noticia', '2013-07-03 13:51:46', NULL),
(7, 'svsdvdsvs', '<p>vdvdsvsv</p>', 'norma', '2013-07-03 15:13:26', NULL),
(8, 'svsdvdsvs', '<p>vdvdsvsv</p>', 'norma', '2013-07-03 15:13:46', NULL),
(9, 'svsdvdsvs', '<p>vdvdsvsv</p>', 'norma', '2013-07-03 15:14:08', NULL),
(10, 'ererere', '<p>ererer</p>', 'norma', '2013-07-03 15:14:23', NULL),
(11, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:19:10', NULL),
(12, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:19:31', NULL),
(13, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:19:39', NULL),
(14, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:20:02', NULL),
(15, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:20:55', NULL),
(16, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:21:57', NULL),
(17, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:22:27', NULL),
(18, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:22:58', NULL),
(19, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:23:01', NULL),
(20, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:23:03', NULL),
(21, 'dsd', '<p>sd</p>', 'norma', '2013-07-03 15:23:14', NULL),
(22, 'dsd', '<p>sd</p>', 'norma', '2013-07-03 15:30:03', NULL),
(23, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 15:30:36', NULL),
(24, 'sdsd', '<p>dssd</p>', 'norma', '2013-07-03 15:32:31', NULL),
(25, 'sdsd', '<p>dssd</p>', 'norma', '2013-07-03 15:33:12', NULL),
(26, 'wcwcw', '<p>sxcwc</p>', 'norma', '2013-07-03 15:34:11', NULL),
(27, 'dsdsds', '<p>sdsd</p>', '1', '2013-07-03 15:35:38', NULL),
(28, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:36:00', NULL),
(29, 'dsdsds', '<p>sdsd</p>', '1', '2013-07-03 15:36:47', NULL),
(30, 'dsdsds', '<p>sdsd</p>', '1', '2013-07-03 15:37:34', NULL),
(31, 'dsdsds', '<p>sdsd</p>', '3', '2013-07-03 15:37:43', NULL),
(32, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:37:59', NULL),
(33, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:38:53', NULL),
(34, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:49:27', NULL),
(35, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:50:09', NULL),
(36, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:53:03', NULL),
(37, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:53:22', NULL),
(38, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:53:26', NULL),
(39, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:53:44', NULL),
(40, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:55:35', NULL),
(41, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:57:07', NULL),
(42, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:58:08', NULL),
(43, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 15:58:48', NULL),
(44, 'sdsdsd', '<p>ds</p>', '1', '2013-07-03 15:59:07', NULL),
(45, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 15:59:18', NULL),
(46, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 15:59:57', NULL),
(47, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:01:12', NULL),
(48, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:01:34', NULL),
(49, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:02:17', NULL),
(50, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:04:42', NULL),
(51, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:04:54', NULL),
(52, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:05:19', NULL),
(53, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:06:01', NULL),
(54, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:08:13', NULL),
(55, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:08:15', NULL),
(56, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:08:23', NULL),
(57, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:09:07', NULL),
(58, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:09:21', NULL),
(59, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:09:33', NULL),
(60, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:10:00', NULL),
(61, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:10:13', NULL),
(62, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:10:28', NULL),
(63, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:15:09', NULL),
(64, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:27:31', NULL),
(65, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:28:33', NULL),
(66, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:30:12', NULL),
(67, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:30:14', NULL),
(68, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 16:30:15', NULL),
(69, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 17:06:47', NULL),
(70, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 17:07:18', NULL),
(71, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 17:07:47', NULL),
(72, 'sdsdsd', '<p>ds</p>', '1', '2013-07-03 17:07:52', NULL),
(73, 'sdsdsd', '<p>ds</p>', '1', '2013-07-03 17:09:06', NULL),
(74, 'bdbdb', '<p>bdbd</p>', '1', '2013-07-03 17:09:39', NULL),
(75, 'bdbdb', '<p>bdbd</p>', '1', '2013-07-03 17:09:54', NULL),
(76, 'bdbdb', '<p>bdbd</p>', '1', '2013-07-03 17:10:02', NULL),
(77, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 17:10:15', NULL),
(78, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 17:11:30', NULL),
(79, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 17:12:38', NULL),
(80, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 17:12:40', NULL),
(81, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 17:14:24', NULL),
(82, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 17:15:18', NULL),
(83, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 17:15:40', NULL),
(84, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 17:16:01', NULL),
(85, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 17:16:18', NULL),
(86, 'dqdq', '<p>wqdqwdqw</p>', '1', '2013-07-03 17:23:48', NULL),
(87, 'dqdq', '<p>wqdqwdqw</p>', '1', '2013-07-03 17:24:02', NULL),
(88, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:24:08', NULL),
(89, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:25:01', NULL),
(90, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:25:16', NULL),
(91, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:26:18', NULL),
(92, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:26:50', NULL),
(93, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:27:11', NULL),
(94, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:27:23', NULL),
(95, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:27:46', NULL),
(96, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:29:36', NULL),
(97, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:30:02', NULL),
(98, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:30:20', NULL),
(99, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:30:56', NULL),
(100, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:31:57', NULL),
(101, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:32:13', NULL),
(102, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:33:00', NULL),
(103, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:33:23', NULL),
(104, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:33:45', NULL),
(105, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:34:29', NULL),
(106, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:35:14', NULL),
(107, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:35:22', NULL),
(108, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:35:49', NULL),
(109, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:36:14', NULL),
(110, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:36:32', NULL),
(111, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:40:21', NULL),
(112, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:41:44', NULL),
(113, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:42:05', NULL),
(114, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:44:56', NULL),
(115, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:45:12', NULL),
(116, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:45:21', NULL),
(117, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:46:28', NULL),
(118, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:46:39', NULL),
(119, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:47:01', NULL),
(120, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:47:56', NULL),
(121, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:48:12', NULL),
(122, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:49:41', NULL),
(123, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:50:54', NULL),
(124, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:51:28', NULL),
(125, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:51:50', NULL),
(126, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:51:58', NULL),
(127, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:52:09', NULL),
(128, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:52:17', NULL),
(129, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 17:53:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tipos_informativos`
--

CREATE TABLE IF NOT EXISTS `tipos_informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tipos_informativos`
--

INSERT INTO `tipos_informativos` (`id`, `nome`, `descricao`) VALUES
(1, 'Normas', 'As normas são informativos periódicos e importantes, que serão enviados quinzenalmente.'),
(2, 'Lembretes', 'Os lembretes são informativos periódicos, que serão enviados semanalmente. Tem o objetivo de lembrar os colaboradores de obrigações.'),
(3, 'Normas jurídicas', 'A norma jurídica é a célula do ordenamento jurídico. É um imperativo de conduta, que coage os sujeitos a se comportarem da forma por ela esperada e desejada.');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `empresas_participantes_grupos`
--
ALTER TABLE `empresas_participantes_grupos`
  ADD CONSTRAINT `foreignkey_empresa_grupo_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `foreignkey_empresa_grupo_2` FOREIGN KEY (`grupo_id`) REFERENCES `grupos_empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `empresa_tipo_informativo`
--
ALTER TABLE `empresa_tipo_informativo`
  ADD CONSTRAINT `foreign_key_empresa_tipo_informativo_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `foreign_key_empresa_tipo_informativo_2` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD CONSTRAINT `funcionario_fk_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `func_tipo_informativo`
--
ALTER TABLE `func_tipo_informativo`
  ADD CONSTRAINT `func_tipo_informativo_ibfk_1` FOREIGN KEY (`funcionario_id`) REFERENCES `funcionarios` (`id`),
  ADD CONSTRAINT `func_tipo_informativo_ibfk_2` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
