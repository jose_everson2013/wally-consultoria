-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Jul 02, 2013 as 10:22 PM
-- Versão do Servidor: 5.5.8
-- Versão do PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `wally`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `senha` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `admins`
--

INSERT INTO `admins` (`id`, `nome`, `usuario`, `senha`) VALUES
(0, 'teste', 'teste', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(100) NOT NULL,
  `nome_fantasia` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `status` enum('a','d','b') NOT NULL DEFAULT 'a',
  `data_cadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='	' AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `empresas`
--

INSERT INTO `empresas` (`id`, `razao_social`, `nome_fantasia`, `usuario`, `senha`, `status`, `data_cadastro`) VALUES
(4, 'sdsd', 'sds', 'tst', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'a', '2013-06-29 21:39:46'),
(5, 'teste', 'teste', 'empresa2', '123', 'a', '2013-07-02 18:43:29');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `data_cadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `funcionario_fk_empresa_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `funcionarios`
--

INSERT INTO `funcionarios` (`id`, `nome`, `email`, `empresa_id`, `data_cadastro`) VALUES
(3, 'sd', 'trs@sdsd.com', 4, '2013-06-30 12:33:01'),
(4, 'teste', 'teste@teste.com.br', 4, '2013-07-02 17:09:06'),
(5, 'teste', 'teste@teste.com.br', 4, '2013-07-02 17:30:41'),
(6, 'teste', 'teste@teste.com.br', 4, '2013-07-02 17:43:08'),
(7, 'teste', 'teste@teste.com.br', 4, '2013-07-02 17:43:51'),
(8, 'teste', 'teste@teste.com.br', 4, '2013-07-02 17:47:19'),
(9, 'teste', 'sdsd@sdsds.com', 4, '2013-07-02 17:48:32'),
(10, 'teste', 'sdsd@sdsds.com', 4, '2013-07-02 18:00:16'),
(11, 'teste', 'sdsd@sdsds.com', 4, '2013-07-02 18:00:58'),
(12, 'teste10', 'sdsd@sdsds.com', 5, '2013-07-02 18:01:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `func_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `func_tipo_informativo` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `funcionario_id` int(11) NOT NULL,
  `tipo_informativo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `func_tipo_informativo`
--

INSERT INTO `func_tipo_informativo` (`id`, `funcionario_id`, `tipo_informativo`) VALUES
(1, 8, 2),
(2, 9, 1),
(3, 9, 2),
(4, 10, 1),
(5, 10, 2),
(6, 11, 1),
(7, 11, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `informativos`
--

CREATE TABLE IF NOT EXISTS `informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `conteudo` text NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('a','b','r') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `informativos`
--

INSERT INTO `informativos` (`id`, `titulo`, `conteudo`, `tipo`, `data_cadastro`, `status`) VALUES
(2, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171', 'A operadora ____(RAZÃO SOCIAL DA OPERADORA)____, inscrita no CNPJ/MF sob o nº _____(CNPJ)____ e registrada na ANS sob o nº ___(REGISTRO)__, vem solicitar á  Agência Nacional de Saúde Suplementar - ANS, autorização para aplicação de reajuste da contraprestação pecunária aos planos individuais e familiares, no máximo no percentual estabelecido pela ANS, conforme previsto na RN nº 171/08, no período compreendido entre os meses de maio/2013 e abril/2014.', NULL, '2013-06-29 21:50:16', NULL),
(3, 'sdsd', '<p>sdsd</p>\r\n', NULL, '2013-07-01 10:04:36', NULL);

--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD CONSTRAINT `funcionario_fk_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
