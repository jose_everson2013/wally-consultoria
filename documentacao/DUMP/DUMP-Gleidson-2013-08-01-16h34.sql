-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 01, 2013 at 07:29 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wally`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `senha` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `nome`, `usuario`, `senha`) VALUES
(1, 'teste', 'teste', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(100) NOT NULL,
  `nome_fantasia` varchar(100) NOT NULL,
  `cnpj` varchar(20) NOT NULL,
  `registro_ans` varchar(7) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `nome_representante` varchar(255) NOT NULL,
  `email_representante` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `status` enum('a','d','b') NOT NULL DEFAULT 'a',
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `redefinicao_senha` enum('s','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='	' AUTO_INCREMENT=9 ;

--
-- Dumping data for table `empresas`
--

INSERT INTO `empresas` (`id`, `razao_social`, `nome_fantasia`, `cnpj`, `registro_ans`, `categoria`, `nome_representante`, `email_representante`, `usuario`, `senha`, `status`, `data_cadastro`, `redefinicao_senha`) VALUES
(4, 'sdsd', 'sds', '786876786', '7868767', 'Saúde', 'sdsdsdsd', 'gleidson@kmcnigro.com.br', 'tst', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'a', '2013-06-30 00:39:46', 'n'),
(6, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '0011155446688779', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-18 20:52:06', 'n'),
(7, 'andlasnjkdn', 'kjndjkasnkdj', 'nkjdnaskjndjkan', 'jkdnjks', 'jkdnjkasndkjn', 'jkdnsakjndjkasnk', 'jndkjsandjksnadksjand', 'agenciakmcnigro', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-26 04:23:21', 'n'),
(8, 'Agência KMCNigro Propaganda e Marketing', 'KMCNigro', '06.990.590/0001-23', '1546515', 'Saúde', 'Gleidson Brito Santana', 'gleidson@kmcnigro.com.br', 'gleidson', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-08-01 19:01:27', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `empresas_participantes_grupos`
--

CREATE TABLE IF NOT EXISTS `empresas_participantes_grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreignkey_empresa_grupo_1_idx` (`empresa_id`),
  KEY `foreignkey_empresa_grupo_2_idx` (`grupo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `empresas_participantes_grupos`
--

INSERT INTO `empresas_participantes_grupos` (`id`, `empresa_id`, `grupo_id`) VALUES
(3, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `empresa_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `empresa_tipo_informativo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `tipo_informativo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreign_key_empresa_tipo_informativo_2_idx` (`tipo_informativo_id`),
  KEY `foreign_key_empresa_tipo_informativo_1_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `empresa_tipo_informativo`
--

INSERT INTO `empresa_tipo_informativo` (`id`, `empresa_id`, `tipo_informativo_id`) VALUES
(3, 6, 1),
(8, 4, 1),
(9, 4, 2),
(10, 7, 1),
(11, 7, 2),
(12, 7, 3),
(13, 8, 1),
(14, 8, 2),
(15, 8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `data_cadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `funcionario_fk_empresa_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `funcionarios`
--

INSERT INTO `funcionarios` (`id`, `nome`, `email`, `empresa_id`, `data_cadastro`) VALUES
(3, 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 4, '2013-06-30 15:33:01'),
(14, 'Gleidson Brito Santana', 'gleidsonbrito.ads@gmail.com', 4, '2013-07-03 17:35:11');

-- --------------------------------------------------------

--
-- Table structure for table `func_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `func_tipo_informativo` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `funcionario_id` int(11) NOT NULL,
  `tipo_informativo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `func_tipo_funcionario_id` (`funcionario_id`),
  KEY `func_tipo_informativo_ibfk_2_idx` (`tipo_informativo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=60 ;

--
-- Dumping data for table `func_tipo_informativo`
--

INSERT INTO `func_tipo_informativo` (`id`, `funcionario_id`, `tipo_informativo_id`) VALUES
(46, 14, 1),
(47, 14, 2),
(58, 3, 1),
(59, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `grupos_empresas`
--

CREATE TABLE IF NOT EXISTS `grupos_empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='				' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `grupos_empresas`
--

INSERT INTO `grupos_empresas` (`id`, `nome`, `data_criacao`) VALUES
(1, 'Seguradoras', '2013-07-24 02:42:56');

-- --------------------------------------------------------

--
-- Table structure for table `informativos`
--

CREATE TABLE IF NOT EXISTS `informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `conteudo` text NOT NULL,
  `tipo_informativo_id` int(11) DEFAULT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` enum('s','r','e') NOT NULL DEFAULT 'r',
  `grupo_empresa_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_informativos_1` (`tipo_informativo_id`),
  KEY `fk_informativos_2_idx` (`grupo_empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=141 ;

--
-- Dumping data for table `informativos`
--

INSERT INTO `informativos` (`id`, `titulo`, `conteudo`, `tipo_informativo_id`, `data_cadastro`, `status`, `grupo_empresa_id`) VALUES
(132, 'Novidades na saúde', '<p>&nbsp;</p>\n<h1>ANS promove semin&aacute;rio sobre mudan&ccedil;as no perfil da popula&ccedil;&atilde;o brasileira</h1>\n<div class="article-content">\n<p>Est&atilde;o abertas as inscri&ccedil;&otilde;es para o semin&aacute;rio &ldquo;As mudan&ccedil;as demogr&aacute;ficas e seus impactos sobre a Sa&uacute;de Suplementar&rdquo;, que a Ag&ecirc;ncia Nacional de Sa&uacute;de Suplementar (ANS) ir&aacute; promover no dia 14 de agosto, das 8h30 &agrave;s 17h, no Rio de Janeiro, para institui&ccedil;&otilde;es que atuam na sa&uacute;de suplementar.</p>\n<p>No evento, ser&atilde;o discutidas as mudan&ccedil;as no perfil da popula&ccedil;&atilde;o brasileira, com destaque para o aumento da expectativa de vida e a queda da fecundidade, que refletem um quadro de envelhecimento. As palestras colocar&atilde;o em debate os impactos que tais mudan&ccedil;as trar&atilde;o, em longo prazo, para os modelos atuais de financiamento do setor.</p>\n<p>O tema fez parte da Agenda Regulat&oacute;ria 2011/2012 da ANS, em que se prop&ocirc;s o estudo do Pacto Intergeracional e da precifica&ccedil;&atilde;o dos planos de Sa&uacute;de.</p>\n<p>Inscri&ccedil;&otilde;es e informa&ccedil;&otilde;es no folder abaixo.</p>\n<p><a href="http://www.ans.gov.br/images/stories/noticias/imagem/20130723convite-email.jpg" target="_blank"><img src="http://www.ans.gov.br/images/stories/noticias/imagem/20130723convite-email.jpg" alt="20130723convite-email" width="550" height="1099" /></a></p>\n</div>\n<p>&nbsp;</p>', NULL, '2013-12-01 16:12:32', 'e', NULL),
(135, 'Reajuste anual de planos de saúde', '<p>\n<h1>Funcionamento ANS na Jornada Mundial da Juventude</h1>\n<div class="article-tools">\n<div class="dataPublicacao">Data de publica&ccedil;&atilde;o:&nbsp;<span class="createdate">Quarta-feira, 17/07/2013</span></div>\n</div>\n<div class="article-content">\n<p>Devido &agrave; realiza&ccedil;&atilde;o da Jornada Mundial da Juventude, na semana de 23 a 28 de julho, o hor&aacute;rio de funcionamento na Sede da ANS e no N&uacute;cleo da Ag&ecirc;ncia no Rio de Janeiro ser&aacute; alterado. No dia 23/07, ter&ccedil;a-feira, as atividades ser&atilde;o encerradas &agrave;s 16h. Nos dias 25/07 e 26/07, quinta e sexta-feira, n&atilde;o haver&aacute; expediente. J&aacute; no dia 29/07, segunda-feira, o funcionamento ser&aacute; ap&oacute;s &agrave;s 12h. O atendimento aos benefici&aacute;rios ser&aacute; feito normalmente pelo Disque ANS (0800 701 9656), das 8h &agrave;s 20h, ou pela Central de Atendimento ao Consumidor no portal da Ag&ecirc;ncia (<a href="http://www.ans.gov.br/" target="_blank">www.ans.gov.br</a>).</p>\n</div>\n</p>', NULL, '2013-12-01 16:12:32', 'e', NULL),
(136, 'Funcionamento ANS na Jornada Mundial da Juventude', '<p>\n<h1>Funcionamento ANS na Jornada Mundial da Juventude</h1>\n<div class="article-tools">\n<div class="dataPublicacao">Data de publica&ccedil;&atilde;o:&nbsp;<span class="createdate">Quarta-feira, 17/07/2013</span></div>\n</div>\n<div class="article-content">\n<p>Devido &agrave; realiza&ccedil;&atilde;o da Jornada Mundial da Juventude, na semana de 23 a 28 de julho, o hor&aacute;rio de funcionamento na Sede da ANS e no N&uacute;cleo da Ag&ecirc;ncia no Rio de Janeiro ser&aacute; alterado. No dia 23/07, ter&ccedil;a-feira, as atividades ser&atilde;o encerradas &agrave;s 16h. Nos dias 25/07 e 26/07, quinta e sexta-feira, n&atilde;o haver&aacute; expediente. J&aacute; no dia 29/07, segunda-feira, o funcionamento ser&aacute; ap&oacute;s &agrave;s 12h. O atendimento aos benefici&aacute;rios ser&aacute; feito normalmente pelo Disque ANS (0800 701 9656), das 8h &agrave;s 20h, ou pela Central de Atendimento ao Consumidor no portal da Ag&ecirc;ncia (<a href="http://www.ans.gov.br/" target="_blank">www.ans.gov.br</a>).</p>\n</div>\n</p>', NULL, '2013-12-01 16:12:32', 'e', NULL),
(137, 'Funcionamento ANS', '<p>&nbsp;</p>\n<h1>Funcionamento ANS na Jornada Mundial da Juventude</h1>\n<div class="article-tools">\n<div class="dataPublicacao">Data de publica&ccedil;&atilde;o:&nbsp;<span class="createdate">Quarta-feira, 17/07/2013</span></div>\n</div>\n<div class="article-content">\n<p>Devido &agrave; realiza&ccedil;&atilde;o da Jornada Mundial da Juventude, na semana de 23 a 28 de julho, o hor&aacute;rio de funcionamento na Sede da ANS e no N&uacute;cleo da Ag&ecirc;ncia no Rio de Janeiro ser&aacute; alterado. No dia 23/07, ter&ccedil;a-feira, as atividades ser&atilde;o encerradas &agrave;s 16h. Nos dias 25/07 e 26/07, quinta e sexta-feira, n&atilde;o haver&aacute; expediente. J&aacute; no dia 29/07, segunda-feira, o funcionamento ser&aacute; ap&oacute;s &agrave;s 12h. O atendimento aos benefici&aacute;rios ser&aacute; feito normalmente pelo Disque ANS (0800 701 9656), das 8h &agrave;s 20h, ou pela Central de Atendimento ao Consumidor no portal da Ag&ecirc;ncia (<a href="http://www.ans.gov.br/" target="_blank">www.ans.gov.br</a>).</p>\n</div>\n<p>&nbsp;</p>', NULL, '2013-12-01 16:12:32', 'e', NULL),
(138, 'Funcionamento ANS na Jornada Mundial da Juventude', '<p>\n<h1>Funcionamento ANS na Jornada Mundial da Juventude</h1>\n<div class="article-tools">\n<div class="dataPublicacao">Data de publica&ccedil;&atilde;o:&nbsp;<span class="createdate">Quarta-feira, 17/07/2013</span></div>\n</div>\n<div class="article-content">\n<p>Devido &agrave; realiza&ccedil;&atilde;o da Jornada Mundial da Juventude, na semana de 23 a 28 de julho, o hor&aacute;rio de funcionamento na Sede da ANS e no N&uacute;cleo da Ag&ecirc;ncia no Rio de Janeiro ser&aacute; alterado. No dia 23/07, ter&ccedil;a-feira, as atividades ser&atilde;o encerradas &agrave;s 16h. Nos dias 25/07 e 26/07, quinta e sexta-feira, n&atilde;o haver&aacute; expediente. J&aacute; no dia 29/07, segunda-feira, o funcionamento ser&aacute; ap&oacute;s &agrave;s 12h. O atendimento aos benefici&aacute;rios ser&aacute; feito normalmente pelo Disque ANS (0800 701 9656), das 8h &agrave;s 20h, ou pela Central de Atendimento ao Consumidor no portal da Ag&ecirc;ncia (<a href="http://www.ans.gov.br/" target="_blank">www.ans.gov.br</a>).</p>\n</div>\n</p>', 2, '2013-08-01 00:00:00', 'e', NULL),
(139, 'Reajuste Financeiro de Mensalidades', '<p>Os crit&eacute;rios para aplica&ccedil;&atilde;o do reajuste financeiro anual das mensalidades dos planos privados de assist&ecirc;ncia &agrave; sa&uacute;de m&eacute;dico-hospitalares, com ou sem cobertura odontol&oacute;gica...</p>\r\n<p>&nbsp;</p>', NULL, '2013-07-30 06:56:07', 's', NULL),
(140, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171', '<p>Os crit&eacute;rios para aplica&ccedil;&atilde;o do reajuste financeiro anual das mensalidades dos planos privados de assist&ecirc;ncia &agrave; sa&uacute;de m&eacute;dico-hospitalares, com ou sem cobertura odontol&oacute;gica...</p>', 1, '2013-08-01 07:02:53', 'e', 1);

-- --------------------------------------------------------

--
-- Table structure for table `relatorios_destinatarios`
--

CREATE TABLE IF NOT EXISTS `relatorios_destinatarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_destinatario` varchar(100) DEFAULT NULL,
  `relatorio_envio_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_relatorios_destinatarios_1` (`relatorio_envio_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `relatorios_destinatarios`
--

INSERT INTO `relatorios_destinatarios` (`id`, `email_destinatario`, `relatorio_envio_id`) VALUES
(9, 'gleidson@kmcnigro.com.br', 5),
(10, 'gleidsonbrito.ads@gmail.com', 5),
(11, 'gleidsonbrito.ads@gmail.com', 5),
(12, 'gleidsonbrito.ads@gmail.com', 5),
(13, 'gleidson@kmcnigro.com.br', 6),
(14, 'gleidsonbrito.ads@gmail.com', 6),
(15, 'gleidson@kmcnigro.com.br', 7),
(16, 'gleidson@kmcnigro.com.br', 7),
(17, 'gleidsonbrito.ads@gmail.com', 7);

-- --------------------------------------------------------

--
-- Table structure for table `relatorios_envios`
--

CREATE TABLE IF NOT EXISTS `relatorios_envios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_envio` datetime DEFAULT NULL,
  `informativo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_relatorios_envios_1` (`informativo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `relatorios_envios`
--

INSERT INTO `relatorios_envios` (`id`, `data_envio`, `informativo_id`) VALUES
(5, '2013-07-25 04:16:26', 137),
(6, '2013-07-25 04:30:25', 138),
(7, '2013-08-01 07:12:46', 140);

-- --------------------------------------------------------

--
-- Table structure for table `tipos_informativos`
--

CREATE TABLE IF NOT EXISTS `tipos_informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tipos_informativos`
--

INSERT INTO `tipos_informativos` (`id`, `nome`, `descricao`) VALUES
(1, 'Normas', 'As normas são informativos periódicos e importantes, que serão enviados quinzenalmente.'),
(2, 'Lembretes', 'Os lembretes são informativos periódicos, que serão enviados semanalmente. Tem o objetivo de lembrar os colaboradores de obrigações.'),
(3, 'Normas jurídicas', 'A norma jurídica é a célula do ordenamento jurídico. É um imperativo de conduta, que coage os sujeitos a se comportarem da forma por ela esperada e desejada.');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `empresas_participantes_grupos`
--
ALTER TABLE `empresas_participantes_grupos`
  ADD CONSTRAINT `foreignkey_empresa_grupo_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `foreignkey_empresa_grupo_2` FOREIGN KEY (`grupo_id`) REFERENCES `grupos_empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `empresa_tipo_informativo`
--
ALTER TABLE `empresa_tipo_informativo`
  ADD CONSTRAINT `foreign_key_empresa_tipo_informativo_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `foreign_key_empresa_tipo_informativo_2` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD CONSTRAINT `funcionario_fk_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `func_tipo_informativo`
--
ALTER TABLE `func_tipo_informativo`
  ADD CONSTRAINT `func_tipo_informativo_ibfk_1` FOREIGN KEY (`funcionario_id`) REFERENCES `funcionarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `func_tipo_informativo_ibfk_2` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `informativos`
--
ALTER TABLE `informativos`
  ADD CONSTRAINT `fk_informativos_2` FOREIGN KEY (`grupo_empresa_id`) REFERENCES `grupos_empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_informativos_1` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `relatorios_destinatarios`
--
ALTER TABLE `relatorios_destinatarios`
  ADD CONSTRAINT `fk_relatorios_destinatarios_1` FOREIGN KEY (`relatorio_envio_id`) REFERENCES `relatorios_envios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `relatorios_envios`
--
ALTER TABLE `relatorios_envios`
  ADD CONSTRAINT `fk_relatorios_envios_1` FOREIGN KEY (`informativo_id`) REFERENCES `informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
