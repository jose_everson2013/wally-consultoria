-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 28, 2013 at 07:55 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wally`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `senha` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `nome`, `usuario`, `senha`) VALUES
(0, 'teste', 'teste', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Table structure for table `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(100) NOT NULL,
  `nome_fantasia` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `status` enum('a','d','b') NOT NULL DEFAULT 'a',
  `data_cadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='	' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `data_cadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `funcionario_fk_empresa_idx` (`empresa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `informativos`
--

CREATE TABLE IF NOT EXISTS `informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `conteudo` text NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('a','b','r') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `informativos`
--

INSERT INTO `informativos` (`id`, `titulo`, `conteudo`, `tipo`, `data_cadastro`, `status`) VALUES
(1, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171', '<p>Período de Aplicação: maio de 2013 a abril de 2014<br /><br />\r\n\r\nPrezados Senhores<br /><br />\r\n\r\nOs critérios para aplicação do reajuste financeiro anual das mensalidades dos planos privados de assistência à saúde médico-hospitalares, com ou sem cobertura odontológica, são aqueles descritos na Resolução Normativa – RN nº 171, editada pela Agência Nacional de Saúde Suplementar – ANS em 29/04/2008 e publicada no D.O.U. de 30/04/2008. <br /><br />\r\n\r\nSobre o assunto, gostaríamos de destacar os seguintes pontos:<br /><br />\r\n		\r\n        <ul>\r\n       		<li>O reajuste das contraprestações pecuniárias dos planos individuais e familiares, médico-hospitalares, com ou sem cobertura odontológica, contratados a partir de 02/01/1999 ou adaptados à Lei nº 9.656/98, somente pode ser aplicado com prévia autorização da ANS;</li>\r\n\r\n       		<li>O índice de reajuste máximo a ser autorizado será divulgado pela ANS, em seu site (http://:www.ans.gov.br).</li>\r\n       </ul></p>', NULL, '0000-00-00 00:00:00', NULL),
(2, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171 		', '<p>A operadora ____(RAZÃO SOCIAL DA OPERADORA)____, inscrita no CNPJ/MF sob o nº _____(CNPJ)____ e registrada na ANS sob o nº ___(REGISTRO)__, vem solicitar à Agência Nacional de Saúde Suplementar - ANS, autorização para aplicação de reajuste da contraprestação pecuniária aos planos individuais e familiares, no máximo no percentual estabelecido pela ANS, conforme previsto na RN nº 171/08, no período compreendido entre os meses de maio/2013 e abril/2014.</p>', NULL, '0000-00-00 00:00:00', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD CONSTRAINT `funcionario_fk_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
