-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 24-Jul-2013 às 02:45
-- Versão do servidor: 5.5.31
-- versão do PHP: 5.4.6-1ubuntu1.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `wally`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `senha` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `admins`
--

INSERT INTO `admins` (`id`, `nome`, `usuario`, `senha`) VALUES
(1, 'teste', 'teste', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(100) NOT NULL,
  `nome_fantasia` varchar(100) NOT NULL,
  `cnpj` varchar(20) NOT NULL,
  `registro_ans` varchar(7) NOT NULL,
  `categoria` varchar(255) NOT NULL,
  `nome_representante` varchar(255) NOT NULL,
  `email_representante` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `status` enum('a','d','b') NOT NULL DEFAULT 'a',
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='	' AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `empresas`
--

INSERT INTO `empresas` (`id`, `razao_social`, `nome_fantasia`, `cnpj`, `registro_ans`, `categoria`, `nome_representante`, `email_representante`, `usuario`, `senha`, `status`, `data_cadastro`) VALUES
(4, 'sdsd', 'sds', '786876786', '7868767', 'Saúde', 'sdsdsdsd', 'gleidson@kmcnigro.com.br', 'tst', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'a', '2013-06-30 00:39:46'),
(6, 'Agência KMCNigro Comunicação integrada', 'Agência KMCNigro', '0011155446688779', '115599', 'Saúde', 'Gleidson Brito', 'gleidson@kmcnigro.com.br', 'agenciakmc', '676b78bb0c6e1bcdaf0da8d388cf7d99bb8b3973', 'a', '2013-07-18 20:52:06');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas_participantes_grupos`
--

CREATE TABLE IF NOT EXISTS `empresas_participantes_grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `grupo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreignkey_empresa_grupo_1_idx` (`empresa_id`),
  KEY `foreignkey_empresa_grupo_2_idx` (`grupo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `empresas_participantes_grupos`
--

INSERT INTO `empresas_participantes_grupos` (`id`, `empresa_id`, `grupo_id`) VALUES
(3, 4, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresa_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `empresa_tipo_informativo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empresa_id` int(11) DEFAULT NULL,
  `tipo_informativo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `foreign_key_empresa_tipo_informativo_2_idx` (`tipo_informativo_id`),
  KEY `foreign_key_empresa_tipo_informativo_1_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `empresa_tipo_informativo`
--

INSERT INTO `empresa_tipo_informativo` (`id`, `empresa_id`, `tipo_informativo_id`) VALUES
(3, 6, 1),
(8, 4, 1),
(9, 4, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `data_cadastro` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `funcionario_fk_empresa_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `funcionarios`
--

INSERT INTO `funcionarios` (`id`, `nome`, `email`, `empresa_id`, `data_cadastro`) VALUES
(3, 'sd', 'gleidson@kmcnigro.com.br', 4, '2013-06-30 15:33:01'),
(13, 'new', 'gleidsonbrito.ads@gmail.com', 4, '2013-07-03 17:35:11'),
(14, 'Gleidson Brito Santana', 'gleidsonbrito.ads@gmail.com', 4, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `func_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `func_tipo_informativo` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `funcionario_id` int(11) NOT NULL,
  `tipo_informativo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `func_tipo_funcionario_id` (`funcionario_id`),
  KEY `func_tipo_informativo_ibfk_2_idx` (`tipo_informativo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Extraindo dados da tabela `func_tipo_informativo`
--

INSERT INTO `func_tipo_informativo` (`id`, `funcionario_id`, `tipo_informativo_id`) VALUES
(37, 3, 1),
(38, 13, 1),
(39, 14, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `grupos_empresas`
--

CREATE TABLE IF NOT EXISTS `grupos_empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `data_criacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='				' AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `grupos_empresas`
--

INSERT INTO `grupos_empresas` (`id`, `nome`, `data_criacao`) VALUES
(1, 'Seguradoras', '2013-07-24 02:42:56');

-- --------------------------------------------------------

--
-- Estrutura da tabela `informativos`
--

CREATE TABLE IF NOT EXISTS `informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `conteudo` text NOT NULL,
  `tipo_informativo_id` int(11) DEFAULT NULL,
  `data_cadastro` datetime NOT NULL,
  `status` enum('a','b','r','e') NOT NULL DEFAULT 'r',
  PRIMARY KEY (`id`),
  KEY `fk_informativos_1` (`tipo_informativo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=133 ;

--
-- Extraindo dados da tabela `informativos`
--

INSERT INTO `informativos` (`id`, `titulo`, `conteudo`, `tipo_informativo_id`, `data_cadastro`, `status`) VALUES
(132, 'Novidades na saúde', '<p>&nbsp;</p>\n<h1>ANS promove semin&aacute;rio sobre mudan&ccedil;as no perfil da popula&ccedil;&atilde;o brasileira</h1>\n<div class="article-content">\n<p>Est&atilde;o abertas as inscri&ccedil;&otilde;es para o semin&aacute;rio &ldquo;As mudan&ccedil;as demogr&aacute;ficas e seus impactos sobre a Sa&uacute;de Suplementar&rdquo;, que a Ag&ecirc;ncia Nacional de Sa&uacute;de Suplementar (ANS) ir&aacute; promover no dia 14 de agosto, das 8h30 &agrave;s 17h, no Rio de Janeiro, para institui&ccedil;&otilde;es que atuam na sa&uacute;de suplementar.</p>\n<p>No evento, ser&atilde;o discutidas as mudan&ccedil;as no perfil da popula&ccedil;&atilde;o brasileira, com destaque para o aumento da expectativa de vida e a queda da fecundidade, que refletem um quadro de envelhecimento. As palestras colocar&atilde;o em debate os impactos que tais mudan&ccedil;as trar&atilde;o, em longo prazo, para os modelos atuais de financiamento do setor.</p>\n<p>O tema fez parte da Agenda Regulat&oacute;ria 2011/2012 da ANS, em que se prop&ocirc;s o estudo do Pacto Intergeracional e da precifica&ccedil;&atilde;o dos planos de Sa&uacute;de.</p>\n<p>Inscri&ccedil;&otilde;es e informa&ccedil;&otilde;es no folder abaixo.</p>\n<p><a href="http://www.ans.gov.br/images/stories/noticias/imagem/20130723convite-email.jpg" target="_blank"><img src="http://www.ans.gov.br/images/stories/noticias/imagem/20130723convite-email.jpg" alt="20130723convite-email" width="550" height="1099" /></a></p>\n</div>\n<p>&nbsp;</p>', 2, '0000-00-00 00:00:00', 'r');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipos_informativos`
--

CREATE TABLE IF NOT EXISTS `tipos_informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tipos_informativos`
--

INSERT INTO `tipos_informativos` (`id`, `nome`, `descricao`) VALUES
(1, 'Normas', 'As normas são informativos periódicos e importantes, que serão enviados quinzenalmente.'),
(2, 'Lembretes', 'Os lembretes são informativos periódicos, que serão enviados semanalmente. Tem o objetivo de lembrar os colaboradores de obrigações.'),
(3, 'Normas jurídicas', 'A norma jurídica é a célula do ordenamento jurídico. É um imperativo de conduta, que coage os sujeitos a se comportarem da forma por ela esperada e desejada.');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `empresas_participantes_grupos`
--
ALTER TABLE `empresas_participantes_grupos`
  ADD CONSTRAINT `foreignkey_empresa_grupo_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `foreignkey_empresa_grupo_2` FOREIGN KEY (`grupo_id`) REFERENCES `grupos_empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `empresa_tipo_informativo`
--
ALTER TABLE `empresa_tipo_informativo`
  ADD CONSTRAINT `foreign_key_empresa_tipo_informativo_1` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `foreign_key_empresa_tipo_informativo_2` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD CONSTRAINT `funcionario_fk_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `func_tipo_informativo`
--
ALTER TABLE `func_tipo_informativo`
  ADD CONSTRAINT `func_tipo_informativo_ibfk_2` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `func_tipo_informativo_ibfk_1` FOREIGN KEY (`funcionario_id`) REFERENCES `funcionarios` (`id`);

--
-- Limitadores para a tabela `informativos`
--
ALTER TABLE `informativos`
  ADD CONSTRAINT `fk_informativos_1` FOREIGN KEY (`tipo_informativo_id`) REFERENCES `tipos_informativos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
