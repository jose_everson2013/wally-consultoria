-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Jun 28, 2013 as 07:19 PM
-- Versão do Servidor: 5.5.8
-- Versão do PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `wally`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(5) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `senha` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `admins`
--

INSERT INTO `admins` (`id`, `nome`, `usuario`, `senha`) VALUES
(0, 'teste', 'teste', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Estrutura da tabela `informativos`
--

CREATE TABLE IF NOT EXISTS `informativos` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `conteudo` longtext NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `informativos`
--

INSERT INTO `informativos` (`id`, `titulo`, `conteudo`, `data`) VALUES
(1, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171', '<p>Período de Aplicação: maio de 2013 a abril de 2014<br /><br />\r\n\r\nPrezados Senhores<br /><br />\r\n\r\nOs critérios para aplicação do reajuste financeiro anual das mensalidades dos planos privados de assistência à saúde médico-hospitalares, com ou sem cobertura odontológica, são aqueles descritos na Resolução Normativa – RN nº 171, editada pela Agência Nacional de Saúde Suplementar – ANS em 29/04/2008 e publicada no D.O.U. de 30/04/2008. <br /><br />\r\n\r\nSobre o assunto, gostaríamos de destacar os seguintes pontos:<br /><br />\r\n		\r\n        <ul>\r\n       		<li>O reajuste das contraprestações pecuniárias dos planos individuais e familiares, médico-hospitalares, com ou sem cobertura odontológica, contratados a partir de 02/01/1999 ou adaptados à Lei nº 9.656/98, somente pode ser aplicado com prévia autorização da ANS;</li>\r\n\r\n       		<li>O índice de reajuste máximo a ser autorizado será divulgado pela ANS, em seu site (http://:www.ans.gov.br).</li>\r\n       </ul></p>', '2013-06-20 23:31:19'),
(2, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171 		', '<p>A operadora ____(RAZÃO SOCIAL DA OPERADORA)____, inscrita no CNPJ/MF sob o nº _____(CNPJ)____ e registrada na ANS sob o nº ___(REGISTRO)__, vem solicitar à Agência Nacional de Saúde Suplementar - ANS, autorização para aplicação de reajuste da contraprestação pecuniária aos planos individuais e familiares, no máximo no percentual estabelecido pela ANS, conforme previsto na RN nº 171/08, no período compreendido entre os meses de maio/2013 e abril/2014.</p>', '2013-06-20 23:44:56');
