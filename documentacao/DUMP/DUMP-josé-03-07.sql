-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: Jul 03, 2013 as 06:15 PM
-- Versão do Servidor: 5.5.8
-- Versão do PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `wally`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `usuario` varchar(30) NOT NULL,
  `senha` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `admins`
--

INSERT INTO `admins` (`id`, `nome`, `usuario`, `senha`) VALUES
(1, 'teste', 'teste', '40bd001563085fc35165329ea1ff5c5ecbdbbeef');

-- --------------------------------------------------------

--
-- Estrutura da tabela `empresas`
--

CREATE TABLE IF NOT EXISTS `empresas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `razao_social` varchar(100) NOT NULL,
  `nome_fantasia` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `senha` varchar(100) NOT NULL,
  `status` enum('a','d','b') NOT NULL DEFAULT 'a',
  `data_cadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='	' AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `empresas`
--

INSERT INTO `empresas` (`id`, `razao_social`, `nome_fantasia`, `usuario`, `senha`, `status`, `data_cadastro`) VALUES
(4, 'sdsd', 'sds', 'tst', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'a', '2013-06-29 21:39:46'),
(5, 'teste', 'teste', 'empresa2', '123', 'a', '2013-07-02 18:43:29');

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionarios`
--

CREATE TABLE IF NOT EXISTS `funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `empresa_id` int(11) NOT NULL,
  `data_cadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `funcionario_fk_empresa_idx` (`empresa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `funcionarios`
--

INSERT INTO `funcionarios` (`id`, `nome`, `email`, `empresa_id`, `data_cadastro`) VALUES
(3, 'sd', 'nosrev09@gmail.com', 4, '2013-06-30 12:33:01'),
(12, 'teste10', 'jose@kmcnigro.com.br', 5, '2013-07-02 18:01:12'),
(13, 'new', 'new@new.com.br', 4, '2013-07-03 14:35:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `func_tipo_informativo`
--

CREATE TABLE IF NOT EXISTS `func_tipo_informativo` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `funcionario_id` int(11) NOT NULL,
  `tipo_informativo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `func_tipo_funcionario_id` (`funcionario_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `func_tipo_informativo`
--

INSERT INTO `func_tipo_informativo` (`id`, `funcionario_id`, `tipo_informativo`) VALUES
(1, 3, 2),
(2, 12, 1),
(3, 12, 2),
(14, 13, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `informativos`
--

CREATE TABLE IF NOT EXISTS `informativos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(250) NOT NULL,
  `conteudo` text NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  `data_cadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('a','b','r') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=130 ;

--
-- Extraindo dados da tabela `informativos`
--

INSERT INTO `informativos` (`id`, `titulo`, `conteudo`, `tipo`, `data_cadastro`, `status`) VALUES
(2, 'Reajuste Financeiro de Mensalidades – Planos Médico-Hospitalares - RN/ANS nº 171', 'A operadora ____(RAZÃO SOCIAL DA OPERADORA)____, inscrita no CNPJ/MF sob o nº _____(CNPJ)____ e registrada na ANS sob o nº ___(REGISTRO)__, vem solicitar á  Agência Nacional de Saúde Suplementar - ANS, autorização para aplicação de reajuste da contraprestação pecunária aos planos individuais e familiares, no máximo no percentual estabelecido pela ANS, conforme previsto na RN nº 171/08, no período compreendido entre os meses de maio/2013 e abril/2014.', 'noticia', '2013-07-03 10:46:25', NULL),
(3, 'sdsd', '<p>sdsd</p>\r\n', 'noticia', '2013-07-03 10:47:15', NULL),
(4, 'sdsd', '<p>sdd</p>', 'norma', '2013-07-03 10:50:53', NULL),
(5, 'bnbnbn', '<p>bnnbn</p>', 'lembrete', '2013-07-03 10:51:35', NULL),
(6, 'asfdsfdsf', '<p>ghnngng</p>', 'noticia', '2013-07-03 10:51:46', NULL),
(7, 'svsdvdsvs', '<p>vdvdsvsv</p>', 'norma', '2013-07-03 12:13:26', NULL),
(8, 'svsdvdsvs', '<p>vdvdsvsv</p>', 'norma', '2013-07-03 12:13:46', NULL),
(9, 'svsdvdsvs', '<p>vdvdsvsv</p>', 'norma', '2013-07-03 12:14:08', NULL),
(10, 'ererere', '<p>ererer</p>', 'norma', '2013-07-03 12:14:23', NULL),
(11, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:19:10', NULL),
(12, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:19:31', NULL),
(13, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:19:39', NULL),
(14, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:20:02', NULL),
(15, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:20:55', NULL),
(16, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:21:57', NULL),
(17, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:22:27', NULL),
(18, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:22:58', NULL),
(19, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:23:01', NULL),
(20, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:23:03', NULL),
(21, 'dsd', '<p>sd</p>', 'norma', '2013-07-03 12:23:14', NULL),
(22, 'dsd', '<p>sd</p>', 'norma', '2013-07-03 12:30:03', NULL),
(23, 'sdsd', '<p>sdsd</p>', 'norma', '2013-07-03 12:30:36', NULL),
(24, 'sdsd', '<p>dssd</p>', 'norma', '2013-07-03 12:32:31', NULL),
(25, 'sdsd', '<p>dssd</p>', 'norma', '2013-07-03 12:33:12', NULL),
(26, 'wcwcw', '<p>sxcwc</p>', 'norma', '2013-07-03 12:34:11', NULL),
(27, 'dsdsds', '<p>sdsd</p>', '1', '2013-07-03 12:35:38', NULL),
(28, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:36:00', NULL),
(29, 'dsdsds', '<p>sdsd</p>', '1', '2013-07-03 12:36:47', NULL),
(30, 'dsdsds', '<p>sdsd</p>', '1', '2013-07-03 12:37:34', NULL),
(31, 'dsdsds', '<p>sdsd</p>', '3', '2013-07-03 12:37:43', NULL),
(32, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:37:59', NULL),
(33, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:38:53', NULL),
(34, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:49:27', NULL),
(35, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:50:09', NULL),
(36, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:53:03', NULL),
(37, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:53:22', NULL),
(38, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:53:26', NULL),
(39, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:53:44', NULL),
(40, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:55:35', NULL),
(41, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:57:07', NULL),
(42, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:58:08', NULL),
(43, 'dsdsds', '<p>sdsd</p>', '2', '2013-07-03 12:58:48', NULL),
(44, 'sdsdsd', '<p>ds</p>', '1', '2013-07-03 12:59:07', NULL),
(45, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 12:59:18', NULL),
(46, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 12:59:57', NULL),
(47, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:01:12', NULL),
(48, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:01:34', NULL),
(49, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:02:17', NULL),
(50, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:04:42', NULL),
(51, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:04:54', NULL),
(52, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:05:19', NULL),
(53, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:06:01', NULL),
(54, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:08:13', NULL),
(55, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:08:15', NULL),
(56, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:08:23', NULL),
(57, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:09:07', NULL),
(58, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:09:21', NULL),
(59, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:09:33', NULL),
(60, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:10:00', NULL),
(61, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:10:13', NULL),
(62, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:10:28', NULL),
(63, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:15:09', NULL),
(64, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:27:31', NULL),
(65, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:28:33', NULL),
(66, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:30:12', NULL),
(67, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:30:14', NULL),
(68, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 13:30:15', NULL),
(69, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 14:06:47', NULL),
(70, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 14:07:18', NULL),
(71, 'sdsdsd', '<p>ds</p>', '2', '2013-07-03 14:07:47', NULL),
(72, 'sdsdsd', '<p>ds</p>', '1', '2013-07-03 14:07:52', NULL),
(73, 'sdsdsd', '<p>ds</p>', '1', '2013-07-03 14:09:06', NULL),
(74, 'bdbdb', '<p>bdbd</p>', '1', '2013-07-03 14:09:39', NULL),
(75, 'bdbdb', '<p>bdbd</p>', '1', '2013-07-03 14:09:54', NULL),
(76, 'bdbdb', '<p>bdbd</p>', '1', '2013-07-03 14:10:02', NULL),
(77, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 14:10:15', NULL),
(78, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 14:11:30', NULL),
(79, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 14:12:38', NULL),
(80, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 14:12:40', NULL),
(81, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 14:14:24', NULL),
(82, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 14:15:18', NULL),
(83, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 14:15:40', NULL),
(84, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 14:16:01', NULL),
(85, 'bdbdb', '<p>bdbd</p>', '2', '2013-07-03 14:16:18', NULL),
(86, 'dqdq', '<p>wqdqwdqw</p>', '1', '2013-07-03 14:23:48', NULL),
(87, 'dqdq', '<p>wqdqwdqw</p>', '1', '2013-07-03 14:24:02', NULL),
(88, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:24:08', NULL),
(89, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:25:01', NULL),
(90, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:25:16', NULL),
(91, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:26:18', NULL),
(92, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:26:50', NULL),
(93, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:27:11', NULL),
(94, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:27:23', NULL),
(95, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:27:46', NULL),
(96, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:29:36', NULL),
(97, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:30:02', NULL),
(98, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:30:20', NULL),
(99, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:30:56', NULL),
(100, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:31:57', NULL),
(101, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:32:13', NULL),
(102, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:33:00', NULL),
(103, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:33:23', NULL),
(104, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:33:45', NULL),
(105, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:34:29', NULL),
(106, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:35:14', NULL),
(107, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:35:22', NULL),
(108, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:35:49', NULL),
(109, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:36:14', NULL),
(110, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:36:32', NULL),
(111, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:40:21', NULL),
(112, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:41:44', NULL),
(113, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:42:05', NULL),
(114, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:44:56', NULL),
(115, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:45:12', NULL),
(116, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:45:21', NULL),
(117, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:46:28', NULL),
(118, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:46:39', NULL),
(119, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:47:01', NULL),
(120, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:47:56', NULL),
(121, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:48:12', NULL),
(122, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:49:41', NULL),
(123, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:50:54', NULL),
(124, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:51:28', NULL),
(125, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:51:50', NULL),
(126, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:51:58', NULL),
(127, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:52:09', NULL),
(128, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:52:17', NULL),
(129, 'dqdq', '<p>wqdqwdqw</p>', '2', '2013-07-03 14:53:06', NULL);

--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `funcionarios`
--
ALTER TABLE `funcionarios`
  ADD CONSTRAINT `funcionario_fk_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para a tabela `func_tipo_informativo`
--
ALTER TABLE `func_tipo_informativo`
  ADD CONSTRAINT `func_tipo_informativo_ibfk_1` FOREIGN KEY (`funcionario_id`) REFERENCES `funcionarios` (`id`);
